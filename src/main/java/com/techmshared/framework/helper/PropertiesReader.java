package com.techmshared.framework.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesReader {

	private static PropertiesReader instance = null;
	private static Properties prop = null;
	private static final Logger LOGGER = Logger
			.getLogger(PropertiesReader.class.getName());

	private PropertiesReader() {

	}

	public static PropertiesReader getInstance() {
		if (instance == null) {
			instance = new PropertiesReader();
			loadProperties();
		}
		return instance;
	}

	public static void loadProperties() {

		prop = new Properties();
		String propFileName = SharedConstants.SHARED_CONFIG_FILE;
		String deviceVariant = DriverHelper.getDeviceVariant();

		if ("Simulator".equalsIgnoreCase(deviceVariant)) {
			propFileName = SharedConstants.SHARED_SIMULATOR_CONFIG_FILE;
		}

		try {
			InputStream inputStream = PropertiesReader.class.getClassLoader()
					.getResourceAsStream(propFileName);
			if (inputStream != null) {
				prop.load(inputStream);
			}

		} catch (IOException ex) {
			LOGGER.debug("Unable to locate config.properties file with Name:"
					+ propFileName, ex);
		}
	}

	public String getProperty(String property) {

		String propertyValue = null;

		if (prop != null) {

			propertyValue = prop.getProperty(property);
		}

		return propertyValue;
	}
}