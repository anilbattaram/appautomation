package com.techmshared.framework.cucumber;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.techmshared.framework.cucumber.bean.Element;
import com.techmshared.framework.cucumber.bean.Feature;
import com.techmshared.framework.helper.SharedConstants;

public class JsonToJava {

	private static final Logger LOGGER = Logger.getLogger(JsonToJava.class
			.getName());
	private List<String> allTestIds = new ArrayList<String>();
	private List<String> passedTestIds = new ArrayList<String>();
	private List<String> failedTestIds = new ArrayList<String>();
	private List<String> skippedTestIds = new ArrayList<String>();
	private List<String> pendingTestIds = new ArrayList<String>();
	private List<String> undefinedTestIds = new ArrayList<String>();
	private List<String> missingTestIds = new ArrayList<String>();
	private List<Element> allScenariosList = new ArrayList<Element>();
	private List<Element> skippedScenariosList = new ArrayList<Element>();
	private List<Element> failedScenariosList = new ArrayList<Element>();
	private List<Element> passedScenariosList = new ArrayList<Element>();

	public void processTestResults() {
		File source = new File(SharedConstants.CUCUMBER_REPORT_FILE);
		InputStream input = null;
		try {
			input = new FileInputStream(source);
			Reader reader = new InputStreamReader(input, SharedConstants.ENCODING);
			Feature[] features = new Gson().fromJson(reader, Feature[].class);
			collectAllStepResults(features);
			processAllSenarioResults();

		} catch (FileNotFoundException ex) {
			LOGGER.debug("Exception Message :" + ex);
		} catch (UnsupportedEncodingException ex) {
			LOGGER.debug("Exception Message :" + ex);
		}
	}

	private void collectAllStepResults(Feature[] features) {
		for (Feature feature : features) {
			allScenariosList.addAll(feature.processAllScenarios());
			failedScenariosList.addAll(feature.processFailedScenarios());
			skippedScenariosList.addAll(feature.processSkippedScenarios());
			passedScenariosList.addAll(feature.processPassedScenarios());
		}
	}

	private void processAllSenarioResults() {
		if (allScenariosList != null) {
			allTestIds.addAll(collectTestRailId(allScenariosList));
		}
		if (passedScenariosList != null) {
			passedTestIds.addAll(collectTestRailId(passedScenariosList));
		}
		if (failedScenariosList != null) {
			failedTestIds.addAll(collectTestRailId(failedScenariosList));
		}
		if (skippedScenariosList != null) {
			skippedTestIds.addAll(collectTestRailId(skippedScenariosList));
		}
	}

	private Set<String> collectTestRailId(List<Element> scenariosList) {
		Set<String> testIdsSet = new HashSet<String>();
		for (Element element : scenariosList) {
			if (!element.getComments().isEmpty()) {
				for (int i = 0; i < element.getComments().size(); i++) {
					if (element.getComments().get(i).getCommentValue()
							.contains(SharedConstants.TESTRAIL_COMMENT_VALUE)) {
						String testRailIds = element.getComments().get(i)
								.getCommentValue();
						String[] testRailIdList = testRailIds.split(",");
						for (int j = 0; j < testRailIdList.length; j++) {
							String testRailId = testRailIdList[j].replaceFirst(
									SharedConstants.HASH_SYMBOL, "").replaceAll("[^\\d-]", "");
							testIdsSet.add(testRailId);
						}
					}
				}
			}
		}
		return testIdsSet;
	}

	public List<String> getAllTestIds() {
		return allTestIds;
	}

	public List<String> getPassedTestIds() {
		return passedTestIds;
	}

	public List<String> getFailedTestIds() {
		return failedTestIds;
	}

	public List<String> getSkippedTestIds() {
		return skippedTestIds;
	}

	public List<String> getPendingTestIds() {
		return pendingTestIds;
	}

	public List<String> getUndefinedTestIds() {
		return undefinedTestIds;
	}

	public List<String> getMissingTestIds() {
		return missingTestIds;
	}

	public List<Element> getFailedScenariosList() {
		return failedScenariosList;
	}
}
