package com.techmshared.framework.cucumber.bean;

import static com.googlecode.totallylazy.Option.option;

import com.googlecode.totallylazy.Sequence;
import com.googlecode.totallylazy.Sequences;
import com.techmshared.framework.cucumber.util.ConfigurationOptions;
import com.techmshared.framework.cucumber.util.Status;


public class Element {

	private String id;
	private String description;
	private String name;
	private Step[] steps;
	private Comment[] comments;
	private Tag[] tags;
	private String keyword;
	
	public Sequence<Step> getSteps() {
        return Sequences.sequence(option(steps).getOrElse(new Step[]{})).realise();
    }
	
	public Sequence<Comment> getComments() {
        return Sequences.sequence(option(comments).getOrElse(new Comment[]{})).realise();
    }

    public Sequence<Tag> getTags() {
        return Sequences.sequence(option(tags).getOrElse(new Tag[]{})).realise();
    }

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public boolean hasSteps() {
		return !getSteps().isEmpty();
	}

	public String getKeyword() {
		return keyword;
	}
	 
	 public Status getStatus() {
        boolean hasNoFailed = getSteps().filter(Step.Predicates.hasStatus(Status.FAILED)).isEmpty();
        Status status = Status.PASSED;
        if (!hasNoFailed) {
        	status = Status.FAILED;
        }

        ConfigurationOptions configuration = ConfigurationOptions.instance();
        if (configuration.skippedFailsBuild()) {
            boolean hasNoSkipped = getSteps().filter(Step.Predicates.hasStatus(Status.SKIPPED)).isEmpty();
            if (!hasNoSkipped) {
            	status = Status.FAILED;
            }
        } else if (configuration.pendingFailsBuild()) {
            boolean hasNoSkipped = getSteps().filter(Step.Predicates.hasStatus(Status.PENDING)).isEmpty();
            if (!hasNoSkipped) {
            	status = Status.FAILED;
            }
        } else if (configuration.undefinedFailsBuild()) {
            boolean hasNoSkipped = getSteps().filter(Step.Predicates.hasStatus(Status.UNDEFINED)).isEmpty();
            if (!hasNoSkipped) {
            	status = Status.FAILED;
            }
        } else if (configuration.missingFailsBuild()) {
            boolean hasNoMissing = getSteps().filter(Step.Predicates.hasStatus(Status.MISSING)).isEmpty();
            if (!hasNoMissing) {
            	status = Status.FAILED;
            }
        }
        return status;
	 }
}
