package com.techmshared.framework.cucumber.bean;


import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.googlecode.totallylazy.Function1;
import com.googlecode.totallylazy.Sequence;
import com.googlecode.totallylazy.Sequences;
import com.googlecode.totallylazy.predicates.LogicalPredicate;
import com.techmshared.framework.cucumber.util.Status;
import com.techmshared.framework.cucumber.util.Util;

public class Step {
	
	private static final Logger LOGGER = Logger.getLogger(Step.class.getName());
	private String name;
	private String keyword;
	private Result[] results;
	private Result result;
	private String line;
	private Comment[] comments;
	private String errorMessageResult;
	
	public String getName() {
		return name;
	}

	public String getKeyword() {
		return keyword;
	}

	public Result[] getResults() {
		return results;
	}
	
	public Sequence<Comment> getComments() {
		return Sequences.sequence(comments).realise();
	}
	
    public Status getStatus() {
        if (result == null) {
        	LOGGER.debug("[WARNING] Line " + line + " : " + "Step is missing Result: " + keyword + " : " + name);
            return Status.MISSING;
        } else {
            return Status.valueOf(result.getStatus().toUpperCase());
        }
    }

    public Long getDuration() {
        if (result == null) {
            return 1L;
        } else {
            return result.getDuration();
        }
    }
    
    public String getErrorMessage() {
    	 String content = "";
         if (getStatus() == Status.FAILED) {
             String errorMessage = result.getErrorMessage();
             if (getStatus() == Status.SKIPPED) {
                 errorMessage = "Mode: Skipped causes Failure<br/><span class=\"skipped\">This step was skipped</span>";
             }
             if (getStatus() == Status.UNDEFINED) {
                 errorMessage = "Mode: Not Implemented causes Failure<br/><span class=\"undefined\">This step is not yet implemented</span>";
             }
             content = getStatus().toHtmlClass() + "<span class=\"step-keyword\">" + keyword + " </span><span class=\"step-name\">" + StringEscapeUtils.escapeHtml(name) + "</span><span class=\"step-duration\">" + Util.formatDuration(result.getDuration()) + "</span><div class=\"step-error-message\"><pre>" + formatError(errorMessage) + "</pre></div>" + Util.closeDiv();
         } else if (getStatus() == Status.MISSING) {
             String errorMessage = "<span class=\"missing\">Result was missing for this step</span>";
             content = getStatus().toHtmlClass() + "<span class=\"step-keyword\">" + keyword + " </span><span class=\"step-name\">" + StringEscapeUtils.escapeHtml(name) + "</span><span class=\"step-duration\"></span><div class=\"step-error-message\"><pre>" + formatError(errorMessage) + "</pre></div>" + Util.closeDiv();
         }
         return content;
    }
    
    private String formatError(String errorMessage) {
        errorMessageResult = errorMessage;
        if (errorMessage != null && !errorMessage.isEmpty()) {
        	errorMessageResult = errorMessage.replaceAll("\\\\n", "<br/>");
        }
        return errorMessageResult;
    }
        
    public static class Predicates {
		private Predicates() {
		        	
		        }

        public static LogicalPredicate<Step> hasStatus(final Status status) {
            return new LogicalPredicate<Step>() {
          //      @Override
                public boolean matches(Step step) {
                    return step.getStatus().equals(status);
                }
            };
        }

        public static Function1<Step, Status> status() {
            return new Function1<Step, Status>() {
          //      @Override
                public Status call(Step step) throws Exception {
                    return step.getStatus();
                }
            };
        }        
    }
}
