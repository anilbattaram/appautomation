package com.techmshared.framework.cucumber.bean;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.totallylazy.Sequence;
import com.googlecode.totallylazy.Sequences;
import com.techmshared.framework.cucumber.util.Status;
import com.techmshared.framework.cucumber.util.StatusCounter;

public class Feature {

	private String id;
	private String description;
	private String name;
	private String keyword;
	private String uri;
	private Element[] elements;

	private String jsonFile = "";
	String deviceName = "";

	public String getDeviceName() {		
		if (jsonFile.split("_").length > 1)
			deviceName = (jsonFile.split("_")[0]).substring(0,
					jsonFile.split("_")[0].length());
		return deviceName;
	}

	public void setJsonFile(String json) {
		this.jsonFile = json;
	}

	public Sequence<Element> getElements() {
		return Sequences.sequence(elements).realise();
	}

	public String getUri() {
		return this.uri;
	}

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public String getKeyword() {
		return keyword;
	}

	public List<Step> processSteps() {
		List<Step> allSteps = new ArrayList<Step>();
		StatusCounter stepsCounter = new StatusCounter();
		long totalDuration = 0L;

		if (elements != null) {
			for (Element element : elements) {				
				if (element.hasSteps()) {
					Sequence<Step> steps = element.getSteps();
					for (Step step : steps) {
						allSteps.add(step);
						stepsCounter.incrementFor(step.getStatus());
						totalDuration = totalDuration + step.getDuration();
					}
				}
			}
		}
		return allSteps;
	}
	
	public List<Element> processFailedScenarios() {
		List<Element> failedScenarios = new ArrayList<Element>();

		if (elements != null) {
			for (Element element : elements) {				
				if (element.getStatus() == Status.FAILED) {
					failedScenarios.add(element);
				}		
			}
		}
		return failedScenarios;
	}
	
	public List<Element> processSkippedScenarios() {
		List<Element> skippedScenarios = new ArrayList<Element>();

		if (elements != null) {
			for (Element element : elements) {				
				if (element.getStatus() == Status.SKIPPED) {
					skippedScenarios.add(element);
				}		
			}
		}
		return skippedScenarios;
	}
	
	public List<Element> processPassedScenarios() {
		List<Element> skippedScenarios = new ArrayList<Element>();

		if (elements != null) {
			for (Element element : elements) {				
				if (element.getStatus() == Status.PASSED) {
					skippedScenarios.add(element);
				}		
			}
		}
		return skippedScenarios;
	}
	
	public List<Element> processAllScenarios() {
		List<Element> allScenarios = new ArrayList<Element>();

		if (elements != null) {
			for (Element element : elements) {
				allScenarios.add(element);
			}
		}
		return allScenarios;
	}	
}
