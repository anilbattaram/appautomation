package com.techmshared.framework.cucumber.util;

import java.util.HashMap;
import java.util.Map;


public class StatusCounter {

    private final Map<Status, Integer> counter = new HashMap<Status, Integer>();
    private int count;

    public StatusCounter() {
        for (Status status : Status.values()) {
            counter.put(status, 0);
        }
    }

    /**
     * Increments status counter by 1.
     * 
     * @param status
     *            status for which the counter should be incremented.
     */
    public void incrementFor(Status status) {
        this.incrementFor(status, 1);
    }

    /**
     * Increments status counter.
     * 
     * @param status
     *            status for which the counter should be incremented.
     * @param incrementBy
     *            by this value counter will be incremented
     */
    public void incrementFor(Status status, int incrementBy) {
    	count = this.counter.get(status);
    	count += incrementBy;
        this.counter.put(status, count);
    }

    /**
     * Gets the counter for given status.
     */
    public int getValueFor(Status status) {
        return this.counter.get(status);
    }
}
