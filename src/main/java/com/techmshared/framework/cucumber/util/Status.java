package com.techmshared.framework.cucumber.util;


public enum Status {

    PASSED("#00CE00"),
    FAILED("#FF0000"),
    SKIPPED("#88AAFF"),
    PENDING("#FBB907"),
    UNDEFINED("#FBB957"),
    MISSING("#FBB9A7");

    Status(String color) {
    }
    
    /** Returns statuses in order they are displayed. */
    public static Status[] getOrderedStatuses() {
        return new Status[] {PASSED, FAILED, SKIPPED, PENDING, UNDEFINED, MISSING };
    }

    /** Returns name of the status as lower case characters. */
    public String getName() {
        return name().toLowerCase();
    }

    /** Returns name of the status starting from upper case character. */
    public String getLabel() {
        return String.valueOf(name().charAt(0)).toUpperCase() + name().substring(1).toLowerCase();
    }

    public String toHtmlClass() {
       
        return "<div class=\"" + this.getName().toLowerCase() + "\">";
    }
}