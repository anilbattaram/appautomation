package com.techmshared.framework.cucumber.util;

import java.util.Map;

import com.techmshared.framework.cucumber.bean.Artifact;

public final class ConfigurationOptions {

	private boolean skippedFailsBuildValue;
	private boolean pendingFailsBuildValue;
	private boolean undefinedFailsBuildValue;
	private boolean missingFailsBuildValue;
	private boolean artifactsEnabledValue;
	private Map<String, Artifact> artifactConfiguration;

	private static ConfigurationOptions configuration;

	public static ConfigurationOptions instance() {
		if (configuration == null) {
			configuration = new ConfigurationOptions();
		}
		return configuration;
	}

	public void setSkippedFailsBuild(boolean skippedFailsBuild) {
		skippedFailsBuildValue = skippedFailsBuild;
	}

	public void setPendingFailsBuild(boolean pendingFailsBuild) {
		pendingFailsBuildValue = pendingFailsBuild;
	}

	public void setUndefinedFailsBuild(boolean undefinedFailsBuild) {
		undefinedFailsBuildValue = undefinedFailsBuild;
	}

	public void setMissingFailsBuild(boolean missngFailsBuild) {
		missingFailsBuildValue = missngFailsBuild;
	}

	public void setArtifactsEnabled(boolean artifactsEnabled) {
		artifactsEnabledValue = artifactsEnabled;
	}

	public void setArtifactConfiguration(Map<String, Artifact> configuration) {
		artifactConfiguration = configuration;
	}

	public boolean skippedFailsBuild() {
		return skippedFailsBuildValue;
	}

	public boolean pendingFailsBuild() {
		return pendingFailsBuildValue;
	}

	public boolean undefinedFailsBuild() {
		return undefinedFailsBuildValue;
	}

	public boolean missingFailsBuild() {
		return missingFailsBuildValue;
	}

	public boolean artifactsEnabled() {
		return artifactsEnabledValue;
	}

	public Map<String, Artifact> artifactConfig() {
		return artifactConfiguration;
	}

}
