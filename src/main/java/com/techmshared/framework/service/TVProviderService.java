package com.techmshared.framework.service;

import java.util.List;

import com.techmshared.framework.helper.ApplicationHelper;
import com.techmshared.framework.helper.SharedConstants;
import com.techmshared.framework.mvpd.MvpdFileProcessor;
import com.techmshared.framework.mvpd.ProviderAuthorization;

public class TVProviderService {

	private MvpdFileProcessor processor;

	public TVProviderService() {

		processor = new MvpdFileProcessor();
	}

	public void generateEncryptedMvpdDetails() {

		processor.generateEncryptedMvpdDetails();
	}

	public ProviderAuthorization getTVProviderAuthorization(
			String providerName, String testScenario) {

		ProviderAuthorization authorization = null;
		List<ProviderAuthorization> authorizationList = null;

		if (providerName == null
				|| providerName
				.equalsIgnoreCase(SharedConstants.DEFAULT_PROVIDER_NAME)) {

			// Random number Logic here
			authorizationList = getEncryptedTVProviderList(testScenario);
			int size = authorizationList.size();
			int position = ApplicationHelper.getRandomNumber(size - 1);
			authorization = authorizationList.get(position);
		} else {

			authorization = processor.getTVProviderAuthorization(providerName,
					testScenario);
		}

		return authorization;
	}

	public List<ProviderAuthorization> getEncryptedTVProviderList(
			String testScenario) {

		return processor.getTVProviderList(testScenario);
	}

	public void generateTVProviderData(String showName, String testScenario) {

		List<ProviderAuthorization> authorizationList = getEncryptedTVProviderList(testScenario);
		processor.writeToFile(showName, testScenario, authorizationList);
	}
}