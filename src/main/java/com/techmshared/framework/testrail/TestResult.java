package com.techmshared.framework.testrail;

import java.util.List;

import com.techmshared.framework.cucumber.bean.Step;

public class TestResult {

	private Step step;
	private List<Step> executedSteps;
	private List<String> testRailIds;

	public TestResult(Step step, List<Step> executedSteps,
			List<String> testRailIds) {

		this.step = step;
		this.executedSteps = executedSteps;
		this.testRailIds = testRailIds;
	}

	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public List<Step> getExecutedSteps() {
		return executedSteps;
	}

	public void setExecutedSteps(List<Step> executedSteps) {
		this.executedSteps = executedSteps;
	}

	public List<String> getTestRailIds() {
		return testRailIds;
	}

	public void setTestRailIds(List<String> testRailIds) {
		this.testRailIds = testRailIds;
	}

}
