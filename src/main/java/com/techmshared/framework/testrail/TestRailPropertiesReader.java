package com.techmshared.framework.testrail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.techmshared.framework.helper.SharedConstants;

public class TestRailPropertiesReader {

	private static TestRailPropertiesReader instance = null;
	private static Properties prop = null;
	private static final Logger LOGGER = Logger
			.getLogger(TestRailPropertiesReader.class.getName());

	private TestRailPropertiesReader() {

	}

	public static TestRailPropertiesReader getInstance() {
		if (instance == null) {
			instance = new TestRailPropertiesReader();
			loadProperties();
		}
		return instance;
	}

	public static void loadProperties() {

		prop = new Properties();

		try {
			InputStream inputStream = TestRailPropertiesReader.class
					.getClassLoader().getResourceAsStream(
							SharedConstants.TESTRAIL_CONFIG_FILE);

			if (inputStream != null) {
				prop.load(inputStream);
			}

		} catch (IOException ex) {
			LOGGER.debug("Unable to locate config.properties file.", ex);
		}
	}

	public String getPropertyValue(String property) {

		String propertyValue = null;

		if (prop != null) {

			propertyValue = prop.getProperty(property);
		}

		return propertyValue;
	}
}
