package com.techmshared.framework.testrail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class TestRailAPIClient {
	private static final Logger LOGGER = Logger
			.getLogger(TestRailAPIClient.class.getName());
	private static String utf = "UTF-8";
	private String userName;
	private String pwd;
	private String mUrl;
	private URL url;
	private String slash = "/";
	private String baseUrl;

	public TestRailAPIClient(String baseUrl) {
		if (!baseUrl.endsWith(slash)) {
			this.baseUrl = baseUrl + slash;
		} else {
			this.baseUrl = baseUrl;
		}

		this.mUrl = this.baseUrl + "index.php?/api/v2/";
	}

	/**
	 * Get/Set User
	 *
	 * Returns/sets the user used for authenticating the API requests.
	 */
	public String getUser() {
		return this.userName;
	}

	public void setUser(String user) {
		this.userName = user;
	}

	/**
	 * Get/Set Password
	 *
	 * Returns/sets the password used for authenticating the API requests.
	 */
	public String getPassword() {
		return this.pwd;
	}

	public void setPassword(String password) {
		this.pwd = password;
	}

	/**
	 * Send Get
	 *
	 * Issues a GET request (read) against the API and returns the result (as
	 * Object, see below).
	 *
	 * Arguments:
	 *
	 * uri The API method to call including parameters (e.g. get_case/1)
	 *
	 * Returns the parsed JSON response as standard object which can either be
	 * an instance of JSONObject or JSONArray (depending on the API method). In
	 * most cases, this returns a JSONObject instance which is basically the
	 * same as java.util.Map.
	 */
	public Object sendGet(String uri) throws IOException, TestRailAPIException {
		return this.sendRequest("GET", uri, null);
	}

	/**
	 * Send POST
	 *
	 * Issues a POST request (write) against the API and returns the result (as
	 * Object, see below).
	 *
	 * Arguments:
	 *
	 * uri The API method to call including parameters (e.g. add_case/1) data
	 * The data to submit as part of the request (e.g., a map)
	 *
	 * Returns the parsed JSON response as standard object which can either be
	 * an instance of JSONObject or JSONArray (depending on the API method). In
	 * most cases, this returns a JSONObject instance which is basically the
	 * same as java.util.Map.
	 */
	public Object sendPost(String uri, Object data) throws IOException,
			TestRailAPIException {
		return this.sendRequest("POST", uri, data);
	}

	private Object sendRequest(String method, String uri, Object data)
			throws IOException, TestRailAPIException {
		url = new URL(this.mUrl + uri);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.addRequestProperty("Content-Type", "application/json");
		String auth = getAuthorization(this.userName, this.pwd);
		conn.addRequestProperty("Authorization", "Basic " + auth);

		if (method == "POST" && data != null) {
			byte[] block = JSONValue.toJSONString(data).getBytes(utf);

			conn.setDoOutput(true);
			OutputStream ostream = conn.getOutputStream();
			ostream.write(block);
			ostream.flush();
		}

		int status = conn.getResponseCode();
		InputStream istream;
		if (status != 200) {
			istream = conn.getErrorStream();
			if (istream == null) {
				throw new TestRailAPIException("TestRail API return HTTP "
						+ status + " (No additional error message received)");
			}
		} else {
			istream = conn.getInputStream();
		}

		String text = "";
		if (istream != null) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					istream, utf));

			String line;
			while ((line = reader.readLine()) != null) {
				text += line;
				text += System.getProperty("line.separator");
			}

			reader.close();
		}

		Object result;
		if (text != "") {
			result = JSONValue.parse(text);
		} else {
			result = new JSONObject();
		}

		if (status != 200) {
			String error = "No additional error message received";
			if (result != null && result instanceof JSONObject) {
				JSONObject obj = (JSONObject) result;
				if (obj.containsKey("error")) {
					error = '"' + (String) obj.get("error") + '"';
				}
			}

			throw new TestRailAPIException("TestRail API returned HTTP "
					+ status + "(" + error + ")");
		}

		return result;
	}

	private static String getAuthorization(String user, String password) {
		try {
			return getBase64((user + ":" + password).getBytes(utf));
		} catch (UnsupportedEncodingException ex) {
			LOGGER.debug("Exception Message :" + ex);
		}

		return "";
	}

	private static String getBase64(byte[] buffer) {
		final char[] map = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
				'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
				'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
				'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
				'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5',
				'6', '7', '8', '9', '+', '/' };

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < buffer.length; i++) {
			byte b0 = buffer[i++];
			byte b1 = 0;
			byte b2 = 0;

			int bytes = 3;
			if (i < buffer.length) {
				b1 = buffer[i++];
				if (i < buffer.length) {
					b2 = buffer[i];
				} else {
					bytes = 2;
				}
			} else {
				bytes = 1;
			}

			int total = (b0 << 16) | (b1 << 8) | b2;

			switch (bytes) {
			case 3:
				sb = getThirdTypeBase(sb, map, total);
				break;

			case 2:
				sb = getSecondTypeBase(sb, map, total);
				break;

			case 1:
				sb = getFirstTypeBase(sb, map, total);
				break;

			default:
				LOGGER.debug("Base64 value :" + sb.toString());
			}
		}

		return sb.toString();
	}

	private static StringBuilder getFirstTypeBase(StringBuilder sb, char[] map,
			int total) {
		sb.append(map[(total >> 18) & 0x3f]);
		sb.append(map[(total >> 12) & 0x3f]);
		sb.append('=');
		sb.append('=');
		return sb;
	}

	private static StringBuilder getSecondTypeBase(StringBuilder sb,
			char[] map, int total) {
		sb.append(map[(total >> 18) & 0x3f]);
		sb.append(map[(total >> 12) & 0x3f]);
		sb.append(map[(total >> 6) & 0x3f]);
		sb.append('=');
		return sb;
	}

	private static StringBuilder getThirdTypeBase(StringBuilder sb, char[] map,
			int total) {
		sb.append(map[(total >> 18) & 0x3f]);
		sb.append(map[(total >> 12) & 0x3f]);
		sb.append(map[(total >> 6) & 0x3f]);
		sb.append(map[total & 0x3f]);
		return sb;
	}
}
