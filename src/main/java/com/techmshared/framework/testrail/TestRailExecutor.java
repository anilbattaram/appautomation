package com.techmshared.framework.testrail;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.techmshared.framework.helper.ApplicationHelper;
import com.techmshared.framework.helper.SharedConstants;

public class TestRailExecutor {

	private static final Logger LOGGER = Logger
			.getLogger(TestRailExecutor.class.getName());
	private TestRailPropertiesReader testRailProperties;

	public TestRailExecutor() {

		testRailProperties = TestRailPropertiesReader.getInstance();
	}

	public long createTestRun(List<String> allTestRailIds) {

		long runId = 0;

		try {
			TestRailAPIClient client = new TestRailAPIClient(
					testRailProperties
							.getPropertyValue(SharedConstants.TESTRAIL_CLIENT_API_KEY));
			client.setUser(ApplicationHelper.decryptValue(testRailProperties
					.getPropertyValue(SharedConstants.TESTRAIL_USERNAME_KEY)));
			client.setPassword(ApplicationHelper.decryptValue(testRailProperties
					.getPropertyValue(SharedConstants.TESTRAIL_PWD_KEY)));			
			Map data = new HashMap();
			data.put(SharedConstants.TESTRAIL_NAME_FIELD, getTestRunName());
			data.put(SharedConstants.TESTRAIL_SUITE_ID_FIELD, testRailProperties
					.getPropertyValue(SharedConstants.TESTRAIL_SUITE_ID_KEY));
			data.put(SharedConstants.TESTRAIL_CASE_ID_FIELD, (ArrayList<String>) allTestRailIds);
			data.put(SharedConstants.TESTRAIL_INCULDE_ALL_FIELD, false);
			JSONObject object = (JSONObject) client
					.sendPost(
							testRailProperties
									.getPropertyValue(SharedConstants.TESTRAIL_SEND_POST_URI_KEY),
							data);
			runId = ((Long) object.get(SharedConstants.TESTRAIL_ID_FIELD)).longValue();

		} catch (IOException ex) {
			LOGGER.debug("Exception Message :" + ex);
		} catch (TestRailAPIException ex) {
			LOGGER.debug("Exception Message :" + ex);
		}

		return runId;
	}

	public void postTestResults(long runId, List<String> passedTestIds,
			List<String> failedTestIds) {

			System.out.println("postTestResults()");
		try {
			JSONObject response;
			TestRailAPIClient client = new TestRailAPIClient(
					testRailProperties
							.getPropertyValue(SharedConstants.TESTRAIL_CLIENT_API_KEY));
			client.setUser(testRailProperties
					.getPropertyValue(SharedConstants.TESTRAIL_USERNAME_KEY));
			client.setPassword(testRailProperties
					.getPropertyValue(SharedConstants.TESTRAIL_PWD_KEY));

			Map updateStatus = new HashMap();
			updateStatus.put(SharedConstants.TESTRAIL_SUITE_ID_FIELD, 1);

			for (String testId : passedTestIds) {
				response = (JSONObject) client.sendPost("add_result_for_case/"
						+ runId + "/" + testId, updateStatus);
			}

			updateStatus.put(SharedConstants.TESTRAIL_SUITE_ID_FIELD, 5);
			for (String testId : failedTestIds) {
				response = (JSONObject) client.sendPost("add_result_for_case/"
						+ runId + "/" + testId, updateStatus);
			}

		} catch (IOException ex) {
			LOGGER.debug("Exception Message :" + ex);
		} catch (TestRailAPIException ex) {
			LOGGER.debug("Exception Message :" + ex);
		}
	}

	public boolean isTestRailEnable() {
		System.out.println("isTestRailEnable()");
		return isEnable(testRailProperties
				.getPropertyValue(SharedConstants.IS_TESTRAIL_ENABLE_KEY));
	}

	private String getTestRunName() {
		System.out.println("getTestRunName()");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(SharedConstants.TESTRAIL_DATE_FORMAT);
		String strDate = sdf.format(cal.getTime());
		return "Test Run for Android : " + strDate;
	}

	private boolean isEnable(String value) {
		System.out.println("isEnable()" + value);
		boolean isEnable = false;
		if ("TRUE".equalsIgnoreCase(value)) {
			isEnable = true;
		}
		return isEnable;
	}
}