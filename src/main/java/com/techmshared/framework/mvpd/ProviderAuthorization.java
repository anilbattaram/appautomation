package com.techmshared.framework.mvpd;

import org.apache.log4j.Logger;

import com.techmshared.framework.helper.AesEncryptor;
import com.techmshared.framework.helper.SharedConstants;

public class ProviderAuthorization {

	private String providerName;
	private String providerUserName;
	private String providerCredential;
	private String testSenario;

	private static final Logger LOGGER = Logger
			.getLogger(ProviderAuthorization.class.getName());

	public ProviderAuthorization() {

	}

	public ProviderAuthorization(String providerName, String providerUserName,
			String providerCredential, String testSenario) {

		this.providerName = providerName;
		this.providerUserName = providerUserName;
		this.providerCredential = providerCredential;
		this.testSenario = testSenario;
	}

	public String getProviderName() {

		return providerName;
	}

	public void setProviderName(String providerName) {

		this.providerName = providerName;
	}

	public String getProviderUserName() {

		return providerUserName;
	}

	public void setProviderUserName(String providerUserName) {

		this.providerUserName = providerUserName;
	}

	public String getProviderCredential() {

		return providerCredential;
	}

	public void setProviderCredential(String providerCredential) {

		this.providerCredential = providerCredential;
	}

	public String getEncryptProviderUserName() {

		return encryptValue(providerUserName);
	}

	public String getEncryptProviderCredential() {

		return encryptValue(providerCredential);
	}

	public String getTestSenario() {

		return testSenario;
	}

	public void setTestSenario(String testSenario) {

		this.testSenario = testSenario;
	}

	private String encryptValue(String value) {

		AesEncryptor encryptor = new AesEncryptor();
		String encryptedValue = null;
		encryptor.setText(value);

		try {
			encryptedValue = encryptor.encrypt();
		} catch (Exception ex) {
			LOGGER.info("Exception while encrypt content: ", ex);
		}
		return encryptedValue;
	}

	@Override
	public String toString() {

		return providerName + SharedConstants.PIPE_SYMBOL
				+ getEncryptProviderUserName() + SharedConstants.PIPE_SYMBOL
				+ getProviderCredential() + SharedConstants.END_OF_LINE_SYMBOL;
	}
}