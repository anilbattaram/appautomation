package com.techmshared.framework.mvpd;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.techmshared.framework.helper.SharedConstants;

public class MvpdFileProcessor {

	private static final Logger LOGGER = Logger
			.getLogger(MvpdFileProcessor.class.getName());
	public MvpdProviderExcelServices excelFile = null;

	public MvpdFileProcessor() {

		excelFile = new MvpdProviderExcelServices();
	}

	public void generateEncryptedMvpdDetails() {

		LOGGER.debug("Reading Providers file :: Start");
		List<ProviderAuthorization> mvpdList = readProvidersFile();
		LOGGER.debug("Reading Providers file :: Completed");

		LOGGER.debug("Writing Encrypted data to File :: Start");
		postEncryptedDetailsToFile(mvpdList);
		LOGGER.debug("Writing Encrypted data to File :: Completed");
	}

	public ProviderAuthorization getTVProviderAuthorization(
			String providerName, String testScenario) {

		return excelFile.getProvider(providerName, testScenario);
	}

	public List<ProviderAuthorization> getTVProviderList(String testScenario) {

		return excelFile.getEncryptedListFromExcel(testScenario);
	}

	private List<ProviderAuthorization> readProvidersFile() {

		List<ProviderAuthorization> mvpdList = excelFile
				.getProvidersListFromExcel();

		return mvpdList;
	}

	private void postEncryptedDetailsToFile(
			List<ProviderAuthorization> mvpdDetails) {
		String fileName = SharedConstants.ENCRYPTED_PROVIDERS_FILE_NAME;
		try {
			int last = 0;
			File file = new File(fileName);
			HSSFWorkbook workbook = null;
			HSSFSheet worksheet = null;
			FileOutputStream fout = null;

			if (file.exists()) {
				file.delete();
			}

			// create new file
			fout = new FileOutputStream(file);

			workbook = new HSSFWorkbook();
			worksheet = workbook.createSheet("Providers");

			for (int i = 0; i < mvpdDetails.size(); i++) {
				last = i + 1;
				if (last != 1) {
					last = worksheet.getLastRowNum() + 1;
				} else {
					HSSFRow rowHead = worksheet.createRow(0);
					HSSFCell cellHeadA = rowHead.createCell((short) 0);
					cellHeadA
							.setCellValue(SharedConstants.EXCEL_PROVIDER_HEADER);
					HSSFCellStyle cellStyle = workbook.createCellStyle();

					HSSFCell cellHeadB = rowHead.createCell((short) 1);
					cellHeadB
							.setCellValue(SharedConstants.EXCEL_USERNAME_HEADER);

					HSSFCell cellHeadC = rowHead.createCell((short) 2);
					cellHeadC.setCellValue(SharedConstants.EXCEL_PWD_HEADER);

					HSSFCell cellHeadD = rowHead.createCell((short) 3);
					cellHeadD
							.setCellValue(SharedConstants.EXCEL_SCENARIO_HEADER);
				}
				HSSFRow row = worksheet.createRow(last);
				HSSFCell cellA = row.createCell((short) 0);
				cellA.setCellValue(mvpdDetails.get(i).getProviderName());

				HSSFCell cellB = row.createCell((short) 1);
				cellB.setCellValue(mvpdDetails.get(i)
						.getEncryptProviderUserName());

				HSSFCell cellC = row.createCell((short) 2);
				cellC.setCellValue(mvpdDetails.get(i)
						.getEncryptProviderCredential());

				HSSFCell cellD = row.createCell((short) 3);
				cellD.setCellValue(mvpdDetails.get(i).getTestSenario());
			}
			workbook.write(fout);
			fout.flush();
			fout.close();
		} catch (FileNotFoundException ex) {
			LOGGER.info("Exception while creating file: ", ex);
		} catch (IOException ex) {
			LOGGER.info("Exception while writing data to file: ", ex);
		}
	}

	public void writeToFile(String showName, String testScenario,
			List<ProviderAuthorization> authorizationList) {

		try {
			File file = new File(getFileName(testScenario));

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			} else {
				file.delete();
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(SharedConstants.EXAMPLE_VALUE);
			bw.write(SharedConstants.END_OF_LINE_SYMBOL);
			bw.write(SharedConstants.TAB_SYMBOL);
			bw.write(SharedConstants.TEXT_FILE_HEADER);
			bw.write(SharedConstants.END_OF_LINE_SYMBOL);

			for (ProviderAuthorization provider : authorizationList) {
				StringBuffer value = new StringBuffer();
				value.append(SharedConstants.PIPE_SYMBOL);
				value.append(showName);
				value.append(SharedConstants.PIPE_SYMBOL);
				value.append(provider.getProviderName());
				value.append(SharedConstants.PIPE_SYMBOL);

				if (value != null) {
					bw.write(SharedConstants.TAB_SYMBOL + value.toString());
					bw.write(SharedConstants.END_OF_LINE_SYMBOL);
				}
			}

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getFileName(String testScenario) {

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(SharedConstants.DATE_FORMAT);
		String strDate = sdf.format(cal.getTime());
		StringBuffer fileName = new StringBuffer();
		if (SharedConstants.NO_AUTH_TEST_SENARIO_VALUE
				.equalsIgnoreCase(testScenario)) {

			fileName.append(SharedConstants.NO_AUTH_PROVIDER_FILE_NAME);
		} else if (SharedConstants.AUTH_TEST_SENARIO_VALUE
				.equalsIgnoreCase(testScenario)) {

			fileName.append(SharedConstants.AUTH_PROVIDER_FILE_NAME);
		}

		fileName.append(strDate);
		fileName.append(SharedConstants.TEXT_FILE_EXTENSION);

		return fileName.toString();
	}
}