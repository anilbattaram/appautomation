package com.techmshared.framework.mvpd;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSElement;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.techmshared.framework.helper.AesEncryptor;
import com.techmshared.framework.helper.ApplicationHelper;
import com.techmshared.framework.helper.DriverHelper;
import com.techmshared.framework.helper.PropertiesReader;
import com.techmshared.framework.helper.SharedConstants;

public class IOSTvproviderHelper implements TvProviderHelperAction {

	AppiumDriver driver;
	private Wait waitVar;
	PropertiesReader properties;
	AesEncryptor encryptor = new AesEncryptor();
	private static final Logger LOGGER = Logger
			.getLogger(IOSTvproviderHelper.class.getName());

	public IOSTvproviderHelper() {

		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		waitVar = new WebDriverWait(driver, Long.parseLong(properties
				.getProperty(HelperConstants.DRIVER_IMPLICIT_SECONDS_WAIT_KEY)));
	}

	public void checkProviderLoginPageDisplayed(String tvProvider) {

		IOSElement passWord;
		WebElement signIn;
		ApplicationHelper.sleep(SharedConstants.PAGE_LOAD_WAIT_TIME);
		WebElement emailPath = (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_TV_PROVIDER_USER_CREDENTIALS_KEY))));
		IOSElement email = (IOSElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_TV_PROVIDER_USER_KEY))));
		Assert.assertTrue(email.isEnabled());
		if (HelperConstants.PROVIDER_LABEL_ATT_U_VERSE
				.equalsIgnoreCase(tvProvider)) {
			passWord = (IOSElement) emailPath
					.findElement(By.name(properties
							.getProperty(HelperConstants.IOS_TV_PROVIDER_PWD_LABEL_KEY)));
			Assert.assertTrue(passWord.isEnabled());
		} else {

			passWord = (IOSElement) emailPath
					.findElement(By.className(properties
							.getProperty(HelperConstants.IOS_TV_PROVIDER_PWD_CLASSNAME_KEY)));
			Assert.assertTrue(passWord.isEnabled());
		}

		if (HelperConstants.PROVIDER_LABEL_OPTIMUM.equalsIgnoreCase(tvProvider)) {
			try {
				driver.manage()
						.timeouts()
						.implicitlyWait(SharedConstants.IMPLICIT_WAIT_TIME,
								TimeUnit.MILLISECONDS);
				signIn = driver
						.findElement(By.xpath(properties
								.getProperty(HelperConstants.IOS_TV_PROVIDER_SIGN_IN_KEY)));
				Assert.assertTrue(signIn.isEnabled());

			} catch (Exception e) {
				LOGGER.debug("Exception if element is not visible " + e);
				signIn = driver
						.findElement(By.xpath(properties
								.getProperty(HelperConstants.IPAD_TV_PROVIDER_SIGN_IN_KEY)));
				Assert.assertTrue(signIn.isEnabled());
			}

		} else if (HelperConstants.PROVIDER_LABEL_CHARTER
				.equalsIgnoreCase(tvProvider)) {

			signIn = emailPath
					.findElement(By.name(properties
							.getProperty(HelperConstants.IOS_TV_PROVIDER_LOGIN_NAME_KEY)));
			Assert.assertTrue(signIn.isEnabled());
		} else {

			signIn = emailPath
					.findElement(By.className(properties
							.getProperty(HelperConstants.IOS_TV_PROVIDER_LOGIN_BUTTON_KEY)));
			Assert.assertTrue(signIn.isDisplayed());
		}
	}

	public void checkInvalidCredentialsPopoutDisplayed() {

		WebElement providerLogo = (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_TV_PROVIDER_LOGO_KEY))));

		WebElement invalidCredentialsalert = (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_TV_PROVIDER_INVALID_CREDENTIALS_KEY))));

		WebElement retunToLoginLink = (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_RETURN_TO_LOGIN_KEY))));

		Assert.assertTrue(providerLogo.isDisplayed());
		Assert.assertTrue(invalidCredentialsalert.isDisplayed());
		Assert.assertTrue(retunToLoginLink.isDisplayed());

	}

	public void chooseTVProviderAndLogin(String tvProvider, String username,
			String password) {

		MobileElement tvProviderElement = (MobileElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By
						.id(tvProvider)));

		ApplicationHelper.scrollTo(tvProviderElement);
		tvProviderElement.click();

		checkTVProviderLogin(username, password, tvProvider);
	}

	public void checkTVProviderLogin(String userName, String password,
			String tvProvider) {

		IOSElement passWordElement;
		WebElement signIn;
		String decryptedUserName = null;
		String decryptedPassword = null;
		try {
			decryptedUserName = encryptor.decrypt(userName);
			decryptedPassword = encryptor.decrypt(password);
		} catch (Exception e) {
			LOGGER.debug(" MVPD Provider secure fields" + e);
		}

		ApplicationHelper.sleep(SharedConstants.PAGE_LOAD_WAIT_TIME);
		WebElement emailPath = (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_TV_PROVIDER_USER_CREDENTIALS_KEY))));
		IOSElement email = (IOSElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_TV_PROVIDER_USER_KEY))));
		email.click();
		email.setValue(decryptedUserName);
		if (HelperConstants.PROVIDER_LABEL_ATT_U_VERSE
				.equalsIgnoreCase(tvProvider)) {
			passWordElement = (IOSElement) emailPath
					.findElement(By.name(properties
							.getProperty(HelperConstants.IOS_TV_PROVIDER_PWD_LABEL_KEY)));
			passWordElement.click();
			passWordElement.sendKeys(decryptedPassword);
		} else {

			passWordElement = (IOSElement) emailPath
					.findElement(By.className(properties
							.getProperty(HelperConstants.IOS_TV_PROVIDER_PWD_CLASSNAME_KEY)));
			passWordElement.click();
			passWordElement.sendKeys(decryptedPassword);
		}

		if (HelperConstants.PROVIDER_LABEL_OPTIMUM.equalsIgnoreCase(tvProvider)) {
			try {
				driver.manage()
						.timeouts()
						.implicitlyWait(SharedConstants.IMPLICIT_WAIT_TIME,
								TimeUnit.MILLISECONDS);
				signIn = driver
						.findElement(By.xpath(properties
								.getProperty(HelperConstants.IOS_TV_PROVIDER_SIGN_IN_KEY)));
				signIn.click();

			} catch (Exception e) {
				LOGGER.debug("Exception if element is not visible " + e);
				signIn = driver
						.findElement(By.xpath(properties
								.getProperty(HelperConstants.IPAD_TV_PROVIDER_SIGN_IN_KEY)));
				signIn.click();
			}

		} else if (HelperConstants.PROVIDER_LABEL_CHARTER
				.equalsIgnoreCase(tvProvider)) {

			signIn = emailPath
					.findElement(By.name(properties
							.getProperty(HelperConstants.IOS_TV_PROVIDER_LOGIN_NAME_KEY)));
			signIn.click();
		} else {

			signIn = emailPath
					.findElement(By.className(properties
							.getProperty(HelperConstants.IOS_TV_PROVIDER_LOGIN_BUTTON_KEY)));
			signIn.click();
		}
	}

	public void checkProviderLoginPageDisplayed() {

	}
}
