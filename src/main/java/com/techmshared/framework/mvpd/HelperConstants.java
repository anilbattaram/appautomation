package com.techmshared.framework.mvpd;

public class HelperConstants {
	
	public static final String DRIVER_IMPLICIT_SECONDS_WAIT_KEY = "DRIVER_IMPLICIT_SECONDS_WAIT";
	public static final String ANDROID_LOGIN_PAGE_KEY="ANDROID_LOGIN_PAGE";
	public static final String ANDROID_TV_PROVIDER_LOGIN_PAGE_TEXT_FIELDS_KEY="ANDROID_TV_PROVIDER_LOGIN_PAGE_TEXT_FIELDS";
	public static final String ANDROID_LOGIN_ELEMENT_LOCATOR_KEY="ANDROID_LOGIN_ELEMENT_LOCATOR";
	public static final String ANDROID_TVPROVIDER_OPTIMUM_LOGIN_PAGE_KEY="ANDROID_TVPROVIDER_OPTIMUM_LOGIN_PAGE";	
	public static final String ANDROID_TV_PROVIDER_CHARTER_KEY="ANDROID_TV_PROVIDER_CHARTER";
	public static final String ANDROID_LOGIN_BUTTON_KEY="ANDROID_LOGIN_BUTTON";
	public static final String ANDROID_TV_PROVIDER_OPTIMUM_KEY="ANDROID_TV_PROVIDER_OPTIMUM";
	public static final String ANDROID_TV_PROVIDER_OPTIMUM_LOGIN_BUTTON_KEY="ANDROID_TV_PROVIDER_OPTIMUM_LOGIN_BUTTON";
	public static final String ANDROID_TV_PROVIDER_LIST_ELEMENT_KEY="ANDROID_TV_PROVIDER_LIST_ELEMENT";
	public static final String ANDROID_TV_PROVIDER_KEY="ANDROID_TV_PROVIDER";
	
	public static final String 	IOS_TV_PROVIDER_USER_CREDENTIALS_KEY="IOS_TV_PROVIDER_USER_CREDENTIALS_PATH";
	public static final String	IOS_TV_PROVIDER_USER_KEY= "IOS_TV_PROVIDER_USER_PATH";
	public static final String	IOS_TV_PROVIDER_PWD_LABEL_KEY="IOS_TV_PROVIDER_PWD_LABEL_NAME";
	public static final String	IOS_TV_PROVIDER_PWD_CLASSNAME_KEY= "IOS_TV_PROVIDER_PWD_CLASSNAME";
	public static final String	IOS_TV_PROVIDER_SIGN_IN_KEY	= "IOS_TV_PROVIDER_SIGN_IN_PATH";
	public static final String	IPAD_TV_PROVIDER_SIGN_IN_KEY="IPAD_TV_PROVIDER_SIGN_IN_PATH";
	public static final String	IOS_TV_PROVIDER_LOGIN_NAME_KEY= "IOS_TV_PROVIDER_LOGIN_BUTTON_NAME";
	public static final String	IOS_TV_PROVIDER_LOGIN_BUTTON_KEY= "IOS_TV_PROVIDER_LOGIN_BUTTON_CLASSNAME"; 
	public static final String	IOS_TV_PROVIDER_LOGO_KEY="IOS_TV_PROVIDER_LOGO_PATH";
	public static final String	IOS_TV_PROVIDER_INVALID_CREDENTIALS_KEY="IOS_TV_PROVIDER_INVALID_CREDENTIALS_PATH";
	public static final String	IOS_RETURN_TO_LOGIN_KEY="IOS_RETURN_TO_LOGIN_PATH";
	
	public static final String PROVIDER_LABEL_CHARTER = "Charter";
	public static final String PROVIDER_LABEL_OPTIMUM = "Optimum";
	public static final String PROVIDER_LABEL_ATT_U_VERSE = "AT&T U-verse";
	
}
