package com.techmshared.framework.mvpd;

import io.appium.java_client.AppiumDriver;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.techmshared.framework.helper.AesEncryptor;
import com.techmshared.framework.helper.ApplicationHelper;
import com.techmshared.framework.helper.DriverHelper;
import com.techmshared.framework.helper.PropertiesReader;
import com.techmshared.framework.helper.SharedConstants;

public class AndroidTvProviderHelper implements TvProviderHelperAction {
	AppiumDriver driver;
	private Wait waitVar;
	PropertiesReader properties;
	AesEncryptor encryptor;
	private static final Logger LOGGER = Logger
			.getLogger(AndroidTvProviderHelper.class.getName());

	public AndroidTvProviderHelper() {

		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		encryptor = new AesEncryptor();
		waitVar = new WebDriverWait(driver, Long.parseLong(properties
				.getProperty(HelperConstants.DRIVER_IMPLICIT_SECONDS_WAIT_KEY)));
	}

	public void checkProviderLoginPageDisplayed() {

		WebElement pageElement = (WebElement) waitVar.until(ExpectedConditions
				.presenceOfElementLocated(By.className(properties
						.getProperty(HelperConstants.ANDROID_LOGIN_PAGE_KEY))));
		Assert.assertTrue(pageElement.isDisplayed());
		List<WebElement> loginElements = pageElement
				.findElements(By.className(properties
						.getProperty(HelperConstants.ANDROID_TV_PROVIDER_LOGIN_PAGE_TEXT_FIELDS_KEY)));
		WebElement emailElement = loginElements.get(0);
		Assert.assertTrue(emailElement.isDisplayed());
		WebElement passwordElement = loginElements.get(1);
		Assert.assertTrue(passwordElement.isDisplayed());
		WebElement logElement = (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.className(properties
						.getProperty(HelperConstants.ANDROID_LOGIN_ELEMENT_LOCATOR_KEY))));
		Assert.assertTrue(logElement.isDisplayed());
	}

	public void checkTVProviderLogin(String userName, String password,
			String provider) {
		WebElement pageElement;
		String decryptedUserName = null;
		String decryptedPassword = null;
		try {

			decryptedUserName = encryptor.decrypt(userName);
			decryptedPassword = encryptor.decrypt(password);

			pageElement = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.className(properties
							.getProperty(HelperConstants.ANDROID_LOGIN_PAGE_KEY))));
			Assert.assertTrue(pageElement.isDisplayed());

		} catch (ElementNotFoundException ex) {
			// for optimum provider it is webkit view,rest of the providers it
			// is android view.
			pageElement = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.className(properties
							.getProperty(HelperConstants.ANDROID_TVPROVIDER_OPTIMUM_LOGIN_PAGE_KEY))));
			Assert.assertTrue(pageElement.isDisplayed());
			LOGGER.debug("Element not found" + ex);
		} catch (Exception ex) {
			LOGGER.debug("Exception occured" + ex);
		}

		ApplicationHelper.sleep(SharedConstants.PAGE_LOAD_WAIT_TIME);
		List<WebElement> loginElements = (List<WebElement>) waitVar
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className(properties
						.getProperty(HelperConstants.ANDROID_TV_PROVIDER_LOGIN_PAGE_TEXT_FIELDS_KEY))));

		if (loginElements != null && !loginElements.isEmpty()) {

			int noOfElements = loginElements.size();
			if (noOfElements > 0 && noOfElements <= 2) {

				ApplicationHelper.sleep(SharedConstants.PAGE_LOAD_WAIT_TIME);
				WebElement emailElement = loginElements.get(0);
				Assert.assertTrue(emailElement.isDisplayed());
				WebElement passwordElement = loginElements.get(1);
				Assert.assertTrue(passwordElement.isDisplayed());
				emailElement.sendKeys(decryptedUserName);
				driver.hideKeyboard();
				driver.tap(SharedConstants.TAP_START_X,
						SharedConstants.TAP_START_Y, SharedConstants.TAP_END_X,
						SharedConstants.TAP_END_Y);
				passwordElement.sendKeys(decryptedPassword);
				driver.hideKeyboard();
			}
		} else {

			Assert.assertTrue(false);
		}

		// The login buttons are varying for different providers
		if (provider.equalsIgnoreCase((properties
				.getProperty(HelperConstants.ANDROID_TV_PROVIDER_CHARTER_KEY)))) {
			WebElement loginButtonElement = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
							.getProperty(HelperConstants.ANDROID_LOGIN_BUTTON_KEY))));
			Assert.assertTrue(loginButtonElement.isDisplayed());
			loginButtonElement.click();
		} else if (provider.equalsIgnoreCase((properties
				.getProperty(HelperConstants.ANDROID_TV_PROVIDER_OPTIMUM_KEY)))) {
			WebElement loginImageElement = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
							.getProperty(HelperConstants.ANDROID_TV_PROVIDER_OPTIMUM_LOGIN_BUTTON_KEY))));
			Assert.assertTrue(loginImageElement.isDisplayed());
			loginImageElement.click();
		} else {
			WebElement logElement = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.className(properties
							.getProperty(HelperConstants.ANDROID_LOGIN_ELEMENT_LOCATOR_KEY))));
			Assert.assertTrue(logElement.isDisplayed());
			logElement.click();
		}

	}

	public void checkProviderLoginPageDisplayed(String tvProvider) {
		// Not Applicable for Android
	}

	public void checkInvalidCredentialsPopoutDisplayed() {
		// Not Applicable for Android

	}

	public void chooseTVProviderAndLogin(String providerName, String userName,
			String password) {
		// Not Applicable for Android
	}
}