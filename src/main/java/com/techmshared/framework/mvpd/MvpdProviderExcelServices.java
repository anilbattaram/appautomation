package com.techmshared.framework.mvpd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.techmshared.framework.helper.SharedConstants;

public class MvpdProviderExcelServices {
	private static final Logger LOGGER = Logger
			.getLogger(MvpdProviderExcelServices.class.getName());

	public List<ProviderAuthorization> getProvidersListFromExcel() {

		FileInputStream inputFile = null;
		List<ProviderAuthorization> providerList = null;

		try {

			inputFile = new FileInputStream(new File(
					SharedConstants.PROVIDERS_FILE_NAME));

			Workbook workbook = WorkbookFactory.create(inputFile);
			providerList = processWorkBook(workbook);
			inputFile.close();
		} catch (FileNotFoundException ex) {
			LOGGER.info("Exception while creating file: ", ex);
		} catch (IOException ex) {
			LOGGER.info("Exception while writing data to file: ", ex);
		} catch (InvalidFormatException ex) {
			LOGGER.info("Invalid format of xls provided input: ", ex);
		}
		return providerList;
	}

	public List<ProviderAuthorization> getEncryptedListFromExcel(
			String testSenario) {

		ProviderAuthorization provider = null;
		List<ProviderAuthorization> providerList = null;

		try {

			providerList = new ArrayList<ProviderAuthorization>();
			Workbook workbook = getWorkBook(SharedConstants.ENCRYPTED_PROVIDERS_FILE_NAME);
			int numberOfSheets = workbook.getNumberOfSheets();

			for (int i = 0; i < numberOfSheets; i++) {
				Sheet sheet = workbook.getSheetAt(i);
				Iterator rowIterator = sheet.iterator();

				while (rowIterator.hasNext()) {
					provider = null;
					Row row = (Row) rowIterator.next();
					provider = processRow(row);
					// check and return
					if (provider != null
							&& provider.getTestSenario().contains(testSenario)) {

						providerList.add(provider);
					}
				}
			}
		} catch (Exception ex) {

			LOGGER.info("Exception while getting filetered list: ", ex);
		}
		return providerList;
	}

	public ProviderAuthorization getProvider(String providerName,
			String testScenario) {

		ProviderAuthorization provider = null;
		ProviderAuthorization returnProvider = null;

		Workbook workbook = getWorkBook(SharedConstants.ENCRYPTED_PROVIDERS_FILE_NAME);

		int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			Sheet sheet = workbook.getSheetAt(i);
			Iterator rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				provider = null;
				returnProvider = null;
				Row row = (Row) rowIterator.next();
				provider = processRow(row);
				// check and return
				if (provider != null
						&& provider.getProviderName().equalsIgnoreCase(
								providerName)
						&& provider.getTestSenario().equalsIgnoreCase(
								testScenario)) {
					returnProvider = provider;
					break;
				}
			}
		}

		return returnProvider;
	}

	private List<ProviderAuthorization> processWorkBook(Workbook workbook) {

		List<ProviderAuthorization> providerList = new ArrayList<ProviderAuthorization>();
		ProviderAuthorization provider = null;
		int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			Sheet sheet = workbook.getSheetAt(i);
			Iterator rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				provider = null;
				Row row = (Row) rowIterator.next();
				provider = processRow(row);
				if (provider != null) {
					providerList.add(provider);
				}
			}
		}

		return providerList;
	}

	private ProviderAuthorization processRow(Row row) {

		String providerName = null;
		String providerUserName = null;
		String providerCredential = null;
		String testScenario = null;
		ProviderAuthorization provider = null;

		if (row.getRowNum() > 0) {
			Iterator cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = (Cell) cellIterator.next();
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					if (cell.getColumnIndex() == 0) {
						providerName = cell.getStringCellValue();
					} else if (cell.getColumnIndex() == 1) {
						providerUserName = cell.getStringCellValue();
					} else if (cell.getColumnIndex() == 2) {
						providerCredential = cell.getStringCellValue();
					} else if (cell.getColumnIndex() == 3) {
						testScenario = cell.getStringCellValue();
					}
				}
			}
			provider = new ProviderAuthorization(providerName,
					providerUserName, providerCredential, testScenario);
		}

		return provider;
	}

	private Workbook getWorkBook(String fileName) {

		InputStream inputFile = null;
		Workbook workbook = null;

		try {
			inputFile = MvpdProviderExcelServices.class
					.getClassLoader()
					.getResourceAsStream(
							SharedConstants.ENCRYPTED_PROVIDERS_ACCESS_FILE_NAME);

			workbook = WorkbookFactory.create(inputFile);
			inputFile.close();
		} catch (FileNotFoundException ex) {
			LOGGER.info("Exception while creating file: ", ex);
		} catch (IOException ex) {
			LOGGER.info("Exception while writing data to file: ", ex);
		} catch (InvalidFormatException ex) {
			LOGGER.info("Invalid format of xls provided input: ", ex);
		}

		return workbook;
	}
}