package com.techmshared.framework.mvpd;

public interface TvProviderHelperAction {
	
	public void checkProviderLoginPageDisplayed();
	
	public void checkProviderLoginPageDisplayed(String tvProvider); 
	
	public void checkInvalidCredentialsPopoutDisplayed(); 
	
	public void chooseTVProviderAndLogin(String tvProvider, String username,
			String password);
	
	public void checkTVProviderLogin(String userName, String password,
			String provider);
}
