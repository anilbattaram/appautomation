package com.techmshared.framework.testcasemanagement;

import com.techmshared.framework.testcasemanagement.testrail.TestRailService;
import com.techmshared.util.SharedConstants;

public class TestCaseManager {
	
	public TestCaseManagmentHandler getTestCaseManagmentHandler(String toolName) {
		
		TestCaseManagmentHandler handler = null;
		
		if (SharedConstants.TESTCASE_MANAGMENT_TESTRAIL_TITLE.equalsIgnoreCase(toolName)) {
			
			handler = new TestRailService();
		}
		
		return handler;
	}
}