package com.techmshared.framework.testcasemanagement;

import java.util.List;

public interface TestCaseManagmentHandler {
	
	public long createTestRun(List<String> allTestRailIds);
	
	public void postTestResults(long runId, List<String> passedTestIds,
			List<String> failedTestIds, List<String> skippedTestIds);

}
