package com.techmshared.framework.testcasemanagement.testrail;
 
public class TestRailAPIException extends Exception
{
	public TestRailAPIException(String message)
	{
		super(message);
	}
}
