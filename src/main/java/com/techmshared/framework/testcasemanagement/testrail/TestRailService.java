package com.techmshared.framework.testcasemanagement.testrail;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.techmshared.framework.testcasemanagement.TestCaseManagmentHandler;
import com.techmshared.util.ApplicationHelper;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.SharedConstants;

public class TestRailService implements TestCaseManagmentHandler {

	private static final Logger LOGGER = Logger.getLogger(TestRailService.class
			.getName());
	private TestRailPropertiesReader testRailProperties;

	public TestRailService() {

		testRailProperties = TestRailPropertiesReader.getInstance();
	}

	public long createTestRun(List<String> allTestRailIds) {

		long runId = 0;

		if (isTestRailEnable()) {

			try {
				TestRailAPIClient client = new TestRailAPIClient(
						testRailProperties
								.getPropertyValue(SharedConstants.TESTRAIL_CLIENT_API_KEY));
				client.setUser(ApplicationHelper.decryptValue(testRailProperties
						.getPropertyValue(SharedConstants.TESTRAIL_USERNAME_KEY)));
				client.setPassword(ApplicationHelper.decryptValue(testRailProperties
						.getPropertyValue(SharedConstants.TESTRAIL_PWD_KEY)));
				Map data = new HashMap();
				data.put(SharedConstants.TESTRAIL_NAME_FIELD, getTestRunName());
				data.put(
						SharedConstants.TESTRAIL_SUITE_ID_FIELD,
						testRailProperties
								.getPropertyValue(SharedConstants.TESTRAIL_SUITE_ID_KEY));
				data.put(SharedConstants.TESTRAIL_CASE_ID_FIELD,
						(ArrayList<String>) allTestRailIds);
				data.put(SharedConstants.TESTRAIL_INCULDE_ALL_FIELD, false);
				JSONObject object = (JSONObject) client
						.sendPost(
								testRailProperties
										.getPropertyValue(SharedConstants.TESTRAIL_SEND_POST_URI_KEY),
								data);
				runId = ((Long) object.get(SharedConstants.TESTRAIL_ID_FIELD))
						.longValue();

			} catch (IOException ex) {
				LOGGER.debug("IOException Message : " + ex);
			} catch (TestRailAPIException ex) {
				LOGGER.debug("TestRailException Message : " + ex);
			}
		}

		return runId;
	}

	public void postTestResults(long runId, List<String> passedTestIds,
			List<String> failedTestIds, List<String> skippedTestIds) {

		if (isTestRailEnable()) {

			try {
				JSONObject response;
				TestRailAPIClient client = new TestRailAPIClient(
						testRailProperties
								.getPropertyValue(SharedConstants.TESTRAIL_CLIENT_API_KEY));
				client.setUser(ApplicationHelper.decryptValue(testRailProperties
						.getPropertyValue(SharedConstants.TESTRAIL_USERNAME_KEY)));
				client.setPassword(ApplicationHelper.decryptValue(testRailProperties
						.getPropertyValue(SharedConstants.TESTRAIL_PWD_KEY)));

				Map updateStatus = new HashMap();
				updateStatus.put(SharedConstants.TESTRAIL_STATUS_ID_FIELD, 1);

				for (String testId : passedTestIds) {
					
					response = (JSONObject) client.sendPost(
							"add_result_for_case/" + runId + "/" + testId,
							updateStatus);
					LOGGER.debug("Passed Test Response"
							+ response.toJSONString());
				}

				updateStatus.put(SharedConstants.TESTRAIL_STATUS_ID_FIELD, 5);
				for (String testId : failedTestIds) {
					
					response = (JSONObject) client.sendPost(
							"add_result_for_case/" + runId + "/" + testId,
							updateStatus);

					LOGGER.debug("Failed Test Response"
							+ response.toJSONString());
				}
				
				updateStatus.put(SharedConstants.TESTRAIL_STATUS_ID_FIELD, 2);
				for (String testId : skippedTestIds) {					
					
					response = (JSONObject) client.sendPost(
							"add_result_for_case/" + runId + "/" + testId,
							updateStatus);

					LOGGER.debug("Failed Test Response"
							+ response.toJSONString());
				}

			} catch (IOException ex) {
				LOGGER.debug("IOException Message :" + ex);
			} catch (TestRailAPIException ex) {
				LOGGER.debug("TestRailException Message :" + ex);
			}
		}
	}

	public boolean isTestRailEnable() {

		return isEnable(testRailProperties
				.getPropertyValue(SharedConstants.IS_TESTRAIL_ENABLE_KEY));
	}

	private String getTestRunName() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(
				SharedConstants.TESTRAIL_DATE_FORMAT);
		String strDate = sdf.format(cal.getTime());

		StringBuilder testRunName = new StringBuilder();
		testRunName.append(SharedConstants.TEST_RUN_NAME_PREFIX);
		testRunName.append(DriverHelper.getDevicePlatForm());
		testRunName.append(SharedConstants.COLON_SYMBOL);
		testRunName.append(strDate);
		return testRunName.toString();
	}

	private boolean isEnable(String value) {
		boolean isEnable = false;
		if ("TRUE".equalsIgnoreCase(value)) {
			isEnable = true;
		}
		return isEnable;
	}
}