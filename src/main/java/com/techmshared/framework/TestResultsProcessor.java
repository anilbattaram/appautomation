package com.techmshared.framework;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.googlecode.totallylazy.Sequence;
import com.techmshared.framework.cucumber.JsonToJava;
import com.techmshared.framework.cucumber.bean.Comment;
import com.techmshared.framework.cucumber.bean.Element;
import com.techmshared.framework.cucumber.bean.Step;
import com.techmshared.framework.cucumber.util.Status;
import com.techmshared.framework.helper.SharedConstants;
import com.techmshared.framework.testrail.TestResult;

public class TestResultsProcessor {
	private static final Logger LOGGER = Logger
			.getLogger(TestResultsProcessor.class.getName());

	private List<Step> executedSteps;
	private ArrayList<String> failedTestIds = new ArrayList<String>();
	private ArrayList<String> passedTestIds = null;
	private List<String> allTestIds = null;

	public TestResultsProcessor() {

	}

	public List<String> getAllTestIds() {

		return allTestIds;
	}

	public List<TestResult> processTestResults() {

		List<TestResult> testResults = new ArrayList<TestResult>();

		JsonToJava jsonToJava = new JsonToJava();
		jsonToJava.processTestResults();

		if (jsonToJava.getFailedScenariosList() != null) {
			for (Element scenarioElement : jsonToJava.getFailedScenariosList()) {
				executedSteps = new ArrayList<Step>();
				if (scenarioElement.hasSteps()) {
					Sequence<Step> allSteps = scenarioElement.getSteps();
					for (Step step : allSteps) {
						executedSteps.add(step);
						if (step.getStatus() == Status.FAILED) {
							List<String> failedTestRailIds = null;
							for (int i = executedSteps.size() - 1; i >= 0; i--) {
								failedTestRailIds = getTestRailId(executedSteps
										.get(i));
								if (!failedTestRailIds.isEmpty()) {
									testResults.add(new TestResult(step,
											executedSteps, failedTestRailIds));
								}
							}
						}
					}
				}
			}
		}

		allTestIds = jsonToJava.getAllTestIds();
		getPassedTestRailIds(allTestIds);

		return testResults;
	}

	private List<String> getTestRailId(Step step) {
		List<String> testIds = new ArrayList<String>();
		for (Comment comment : step.getComments()) {
			if ((comment.getCommentValue())
					.contains(SharedConstants.TESTRAIL_COMMENT_VALUE)) {
				String testRailIds = comment.getCommentValue();
				String[] testRailIdList = testRailIds.split(",");
				for (int j = 0; j < testRailIdList.length; j++) {
					String testRailId = testRailIdList[j].replaceFirst(
							SharedConstants.HASH_SYMBOL, "").replaceAll(
							"[^\\d-]", "");
					testIds.add(testRailId);
					failedTestIds.add(testRailId);
				}
			}
		}

		return testIds;
	}

	private List<String> getPassedTestRailIds(List<String> testIds) {

		passedTestIds = new ArrayList<String>();
		passedTestIds.addAll(testIds);
		passedTestIds.removeAll(failedTestIds);
		return passedTestIds;
	}

	public List<String> getFailedTestRailIds() {

		return failedTestIds;
	}

	public List<String> getPassedTestRailIds() {

		return passedTestIds;
	}
}