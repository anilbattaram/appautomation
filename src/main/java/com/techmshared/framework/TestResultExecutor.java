package com.techmshared.framework;

import java.util.List;

import junit.framework.JUnit4TestAdapter;

import org.junit.Test;

import com.techmshared.framework.jira.JiraExecutor;
import com.techmshared.framework.testrail.TestRailExecutor;
import com.techmshared.framework.testrail.TestResult;

public class TestResultExecutor {

	@Test
	public void processResults() {

		TestResultsProcessor resultsProcessor = new TestResultsProcessor();
		TestRailExecutor testRailExecutor = new TestRailExecutor();
		JiraExecutor jiraExecutor = new JiraExecutor();

		List<TestResult> testResults = resultsProcessor.processTestResults();
		List<String> allTestRailIds = resultsProcessor.getAllTestIds();
		for(int i=0;i<=allTestRailIds.size();i++){
			System.out.println("allTestRailIds>>>>>>>>>>"+allTestRailIds.get(i));
		}
		
		List<String> allPassedTestRailIds = resultsProcessor
				.getPassedTestRailIds();
		for(int i=0;i<=allPassedTestRailIds.size();i++){
			System.out.println("allPassedTestRailIds>>>>>>>>>"+allPassedTestRailIds.get(i));
		}
		List<String> allFailedTestRailIds = resultsProcessor
				.getFailedTestRailIds();
		for(int i=0;i<=allFailedTestRailIds.size();i++){
			System.out.println("allFailedTestRailIds>>>>>"+allFailedTestRailIds.get(i));
		}
		if (testRailExecutor.isTestRailEnable()) {
			System.out.println("testRailExecutor.isTestRailEnable():"+testRailExecutor.isTestRailEnable());
			long runId = testRailExecutor.createTestRun(allTestRailIds);
			testRailExecutor.postTestResults(runId, allPassedTestRailIds,
					allFailedTestRailIds);
		}

		if (jiraExecutor.isJiraEnable()) {
			System.out.println("testResults>>>>"+testResults);
			jiraExecutor.postDefectsToJira(testResults);
		}
	}

	public static junit.framework.Test suite() {
		System.out.println("junit.framework.Test suite()");
		return new JUnit4TestAdapter(TestResultExecutor.class);
	}
}