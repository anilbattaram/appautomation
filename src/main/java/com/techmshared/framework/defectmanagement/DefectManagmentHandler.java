package com.techmshared.framework.defectmanagement;

import java.util.List;

import com.techmshared.util.TestResult;

public interface DefectManagmentHandler {
	
	public void postDefects(List<TestResult> testResults);

}
