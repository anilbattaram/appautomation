package com.techmshared.framework.defectmanagement;

import com.techmshared.framework.defectmanagement.jira.JiraService;
import com.techmshared.util.SharedConstants;

public class DefectManager {
	
	public DefectManagmentHandler getDefectManagmentHandler(String defectTrackerName) {
		
		DefectManagmentHandler handler = null;
		
		if (SharedConstants.DEFECT_MANAGMENT_JIRA_TITLE.equalsIgnoreCase(defectTrackerName)) {
			
			handler = new JiraService();
		}
		
		return handler;
	}
}