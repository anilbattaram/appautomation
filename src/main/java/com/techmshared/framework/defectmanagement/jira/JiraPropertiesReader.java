package com.techmshared.framework.defectmanagement.jira;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.techmshared.util.SharedConstants;

public class JiraPropertiesReader {
	private static JiraPropertiesReader instance = null;
	private static Properties prop = null;
	private static final Logger LOGGER = Logger
			.getLogger(JiraPropertiesReader.class.getName());

	private JiraPropertiesReader() {

	}

	public static JiraPropertiesReader getInstance() {
		if (instance == null) {
			instance = new JiraPropertiesReader();
			loadProperties();
		}
		return instance;
	}

	public static void loadProperties() {

		prop = new Properties();

		try {
			InputStream inputStream = JiraPropertiesReader.class
					.getClassLoader().getResourceAsStream(
							SharedConstants.JIRA_CONFIG_FILE);

			if (inputStream != null) {
				prop.load(inputStream);
			}

		} catch (IOException ex) {
			LOGGER.debug("Unable to locate config.properties file.", ex);
		}
	}

	public String getPropertyValue(String property) {

		String propertyValue = null;

		if (prop != null) {

			propertyValue = prop.getProperty(property);
		}

		return propertyValue;
	}
}