package com.techmshared.framework.defectmanagement.jira;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.htmlcleaner.HtmlCleaner;
import org.json.simple.JSONObject;

import com.techmshared.framework.defectmanagement.DefectManagmentHandler;
import com.techmshared.framework.testexecution.cucumber.model.Step;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.HttpRestClient;
import com.techmshared.util.SharedConstants;
import com.techmshared.util.TestResult;

public class JiraService implements DefectManagmentHandler {

	private JiraPropertiesReader jiraProperties;

	public JiraService() {

		jiraProperties = JiraPropertiesReader.getInstance();
	}
	
	public void postDefects(List<TestResult> testResults) {

		if (isJiraEnable()) {
			for (TestResult testResult : testResults) {				
				createJiraDefects(testResult.getStep(), testResult.getExecutedSteps(),
						testResult.getTestReferenceIds());
			}
		}		
	}

	private void createJiraDefects(Step currentStep, List<Step> failedStep,
			List<String> testIds) {

		for (String testId : testIds) {
			System.out.println("logging for: "+testId);
			HttpRestClient jiraClient = new HttpRestClient(
					jiraProperties
					.getPropertyValue(SharedConstants.JIRA_BASE_URL_KEY),
					jiraProperties
					.getPropertyValue(SharedConstants.JIRA_PATH_KEY));
			JSONObject requestObject = new JSONObject();
			String issueDescription;
			StringBuilder description = new StringBuilder();

			description
			.append(SharedConstants.TESTRAIL_REFERENCE_PATH);
			description.append(testId);
			description.append(SharedConstants.CLOSE_SQUARE_BRACKETS_SYMBOL);
			description.append(SharedConstants.END_OF_LINE_SYMBOL);
			for (Step step : failedStep) {
				description.append(SharedConstants.END_OF_LINE_SYMBOL);
				description.append(step.getName());
			}
			description.append(SharedConstants.END_OF_LINE_SYMBOL);
			description.append(SharedConstants.ERROR_MESSAGE_VALUE);
			description.append(removeHtmlFrom(currentStep.getErrorMessage()));
			issueDescription = description.toString();

			Map keyMap = new HashMap();
			keyMap.put(SharedConstants.JIRA_KEY_FIELD, jiraProperties
					.getPropertyValue(SharedConstants.JIRA_PROJECT_KEY));

			Map issuetype = new HashMap();
			issuetype.put(SharedConstants.JIRA_NAME_FIELD, jiraProperties
					.getPropertyValue(SharedConstants.JIRA_ISSUE_TYPE_KEY));

			Map assignee = new HashMap();
			assignee.put(SharedConstants.JIRA_NAME_FIELD, jiraProperties
					.getPropertyValue(SharedConstants.JIRA_ASSIGNEE_KEY));

			/**Map platform = new HashMap();
			platform.put(SharedConstants.JIRA_VALUE_FIELD, DriverHelper.getDevicePlatForm());
			List<Map> platformValue = new ArrayList<Map>();
			platformValue.add(platform);**/

			Map priority = new HashMap();
			priority.put(SharedConstants.JIRA_NAME_FIELD, jiraProperties
					.getPropertyValue(SharedConstants.JIRA_PRIORITY_KEY));

			Map fieldMap = new HashMap();
			fieldMap.put(SharedConstants.JIRA_ISSUE_FIELD_PROJECT, keyMap);
			fieldMap.put(SharedConstants.JIRA_ISSUE_FIELD_SUMMARY,
					currentStep.getName());
			fieldMap.put(SharedConstants.JIRA_ISSUE_FIELD_DESCRIPTION,
					issueDescription);
			fieldMap.put(SharedConstants.JIRA_ISSUE_FIELD_ISSUETYPE, issuetype);
			fieldMap.put(SharedConstants.JIRA_ISSUE_FIELD_PRIORITY, priority);
			fieldMap.put(SharedConstants.JIRA_ISSUE_FIELD_ASSIGNEE, assignee);
			/*fieldMap.put(SharedConstants.JIRA_ISSUE_FIELD_CUSTOMFIELD_PLATFORM,
					(ArrayList<Map>) platformValue);*/
			requestObject.put(SharedConstants.JIRA_ISSUE_FIELDS, fieldMap);

			jiraClient.postJsonRequest(requestObject.toJSONString());
			System.out.println(requestObject.toJSONString());
		}
	}

	private CharSequence removeHtmlFrom(String html) {

		return new HtmlCleaner().clean(html).getText();
	}

	public boolean isJiraEnable() {

		return isEnable(jiraProperties
				.getPropertyValue(SharedConstants.IS_JIRA_ENABLE_KEY));
	}

	private boolean isEnable(String value) {
		boolean isEnable = false;
		if ("TRUE".equalsIgnoreCase(value)) {
			isEnable = true;
		}
		return isEnable;
	}
}