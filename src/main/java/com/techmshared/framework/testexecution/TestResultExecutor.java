package com.techmshared.framework.testexecution;

import java.util.List;

import junit.framework.JUnit4TestAdapter;

import org.junit.Test;

import com.techmshared.framework.defectmanagement.DefectManager;
import com.techmshared.framework.defectmanagement.DefectManagmentHandler;
import com.techmshared.framework.testcasemanagement.TestCaseManager;
import com.techmshared.framework.testcasemanagement.TestCaseManagmentHandler;
import com.techmshared.util.SharedConstants;
import com.techmshared.util.TestResult;

public class TestResultExecutor {

	@Test
	public void processResults() {		

		TestResultsProcessor resultsProcessor = new TestResultsProcessor();
		TestCaseManagmentHandler testCaseManagementHandler = new TestCaseManager().getTestCaseManagmentHandler(SharedConstants.TESTCASE_MANAGMENT_TESTRAIL_TITLE);
		DefectManagmentHandler defectManagmentHandler = new DefectManager().getDefectManagmentHandler(SharedConstants.DEFECT_MANAGMENT_JIRA_TITLE);

		List<TestResult> testResults = resultsProcessor.processTestResults();
		
		List<String> allTestRailIds = resultsProcessor.getAllTestRefIds();
		List<String> allPassedTestRailIds = resultsProcessor
				.getPassedTestReferenceIds();
		List<String> allFailedTestRailIds = resultsProcessor
				.getFailedTestReferenceIds();
		List<String> allSkippedTestRailIds = resultsProcessor
				.getSkippedRefIds();
		
		System.out.println("##########allTestIds:"+allTestRailIds);
		System.out.println("##########allPassedTestds:"+allPassedTestRailIds);
		System.out.println("##########allFailedTestds:"+allFailedTestRailIds);

		long runId = testCaseManagementHandler.createTestRun(allTestRailIds);	
		
		testCaseManagementHandler.postTestResults(runId, allPassedTestRailIds,
				allFailedTestRailIds, allSkippedTestRailIds);
		
		defectManagmentHandler.postDefects(testResults);		
	}

	public static junit.framework.Test suite() {

		return new JUnit4TestAdapter(TestResultExecutor.class);
	}
}