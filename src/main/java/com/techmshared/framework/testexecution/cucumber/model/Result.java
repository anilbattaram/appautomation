package com.techmshared.framework.testexecution.cucumber.model;

public class Result {

    private String status;
    private String error_message;
    private Long duration;

    public String getStatus() {
        return status;
    }

    public Long getDuration() {
        return duration == null ? 0L : duration;
    }

    public String getErrorMessage() {
        return error_message;
    }
}
