package com.techmshared.framework.testexecution;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.googlecode.totallylazy.Sequence;
import com.techmshared.framework.testexecution.cucumber.model.Comment;
import com.techmshared.framework.testexecution.cucumber.model.Element;
import com.techmshared.framework.testexecution.cucumber.model.Feature;
import com.techmshared.framework.testexecution.cucumber.model.Step;
import com.techmshared.framework.testexecution.cucumber.util.Status;
import com.techmshared.util.SharedConstants;
import com.techmshared.util.TestResult;

public class CucumberJsonResultProcessor {

	private static final Logger LOGGER = Logger.getLogger(CucumberJsonResultProcessor.class
			.getName());
	private List<String> allTestReferenceIds = new ArrayList<String>();
	private List<String> passedTestReferenceIds = new ArrayList<String>();
	private List<String> failedTestReferenceIds = new ArrayList<String>();
	private List<String> skippedTestReferenceIds = new ArrayList<String>();
	private List<String> pendingTestReferenceIds = new ArrayList<String>();
	private List<String> undefinedTestReferenceIds = new ArrayList<String>();
	private List<String> missingTestReferenceIds = new ArrayList<String>();
	private List<Element> allScenariosList = new ArrayList<Element>();
	private List<Element> skippedScenariosList = new ArrayList<Element>();
	private List<Element> failedScenariosList = new ArrayList<Element>();
	private List<Element> passedScenariosList = new ArrayList<Element>();
	
	private String reportFileName;

	public CucumberJsonResultProcessor(String fileName) {
		reportFileName = fileName;
	}
	
	public void processTestResults() {
		File source = new File(reportFileName);
		InputStream input = null;
		try {
			input = new FileInputStream(source);
			Reader reader = new InputStreamReader(input, SharedConstants.ENCODING);
			Feature[] features = new Gson().fromJson(reader, Feature[].class);
			collectAllStepResults(features);
			processAllSenarioResults();

		} catch (FileNotFoundException ex) {
			LOGGER.debug("Exception Message :" + ex);
		} catch (UnsupportedEncodingException ex) {
			LOGGER.debug("Exception Message :" + ex);
		}
	}

	private void collectAllStepResults(Feature[] features) {
		for (Feature feature : features) {
			allScenariosList.addAll(feature.processAllScenarios());
			failedScenariosList.addAll(feature.processFailedScenarios());
			skippedScenariosList.addAll(feature.processSkippedScenarios());
			passedScenariosList.addAll(feature.processPassedScenarios());
		}
	}

	private void processAllSenarioResults() {
		if (allScenariosList != null) {
			allTestReferenceIds.addAll(collectAllTestReferences(allScenariosList));
		}
		if (passedScenariosList != null) {
			passedTestReferenceIds.addAll(collectAllTestReferences(passedScenariosList));
		}
		if (failedScenariosList != null) {
			failedTestReferenceIds.addAll(collectAllTestReferences(failedScenariosList));
		}
		if (skippedScenariosList != null) {
			skippedTestReferenceIds.addAll(collectAllTestReferences(skippedScenariosList));
		}
	}

	private Set<String> collectAllTestReferences(List<Element> scenariosList) {
		Set<String> testReferenceIds = new HashSet<String>();
		for (Element element : scenariosList) {
			Sequence<Step> allSteps = element.getSteps();
			for (Step step : allSteps) {
				Sequence<Comment> allComments = step.getComments();
				for (Comment comment : allComments) {
				
					testReferenceIds.addAll(splitTestReferences(comment.getCommentValue()));
				}				
			}			
		}
		return testReferenceIds;
	}
	
	private Set<String> splitTestReferences(String comment) {
		Set<String> testReferenceIds = new HashSet<String>();
		if (comment.contains(SharedConstants.TEST_REFERENCE_COMMENT_TITLE)) {
			String testRefComment = comment;
			String[] testRefIdList = testRefComment.split(",");
			for (int j = 0; j < testRefIdList.length; j++) {				
				String testRailId = testRefIdList[j].replaceFirst(
						SharedConstants.HASH_SYMBOL, "").replaceAll("JIRA:", "");
				testReferenceIds.add(testRailId);				
			}
		}
		return testReferenceIds;
	}

	public List<String> getAllTestReferenceIds() {
		return allTestReferenceIds;
	}

	public List<String> getPassedTestReferenceIds() {
		return passedTestReferenceIds;
	}

	public List<String> getFailedTestReferenceIds() {
		return failedTestReferenceIds;
	}

	public List<String> getSkippedTestReferenceIds() {
		return skippedTestReferenceIds;
	}

	public List<String> getPendingTestReferenceIds() {
		return pendingTestReferenceIds;
	}

	public List<String> getUndefinedTestReferenceIds() {
		return undefinedTestReferenceIds;
	}

	public List<String> getMissingTestReferenceIds() {
		return missingTestReferenceIds;
	}

	public List<Element> getFailedScenariosList() {
		return failedScenariosList;
	}
}