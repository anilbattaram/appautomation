package com.techmshared.framework.testexecution;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.totallylazy.Sequence;
import com.techmshared.framework.testexecution.cucumber.model.Comment;
import com.techmshared.framework.testexecution.cucumber.model.Element;
import com.techmshared.framework.testexecution.cucumber.model.Step;
import com.techmshared.framework.testexecution.cucumber.util.Status;
import com.techmshared.util.SharedConstants;
import com.techmshared.util.TestResult;

public class TestResultsProcessor {

	private List<Step> executedSteps;
	private List<String> failedTestRefIds;
	private List<String> passedTestRefIds;
	private List<String> skippedTestRefIds;
	private List<String> allTestRefIds;

	public TestResultsProcessor() {

	}

	public List<String> getAllTestRefIds() {

		return allTestRefIds;
	}
	
	public List<String> getSkippedRefIds() {

		return skippedTestRefIds;
	}

	public List<TestResult> processTestResults() {

		failedTestRefIds = new ArrayList<String>();
		skippedTestRefIds = new ArrayList<String>();
		List<TestResult> testResults = new ArrayList<TestResult>();

		CucumberJsonResultProcessor cucumberJsonResultProcessor = new CucumberJsonResultProcessor(SharedConstants.CUCUMBER_REPORT_FILE);
		cucumberJsonResultProcessor.processTestResults();

		if (cucumberJsonResultProcessor.getFailedScenariosList() != null) {
			for (Element scenarioElement : cucumberJsonResultProcessor.getFailedScenariosList()) {
				executedSteps = new ArrayList<Step>();
				if (scenarioElement.hasSteps()) {
					testResults = collectFailedStepData(scenarioElement);
				}
			}
		}
		allTestRefIds = cucumberJsonResultProcessor.getAllTestReferenceIds();		
		processPassedTests(allTestRefIds);

		return testResults;
	}
	
	private List<TestResult> collectFailedStepData(Element element) {
		Sequence<Step> allSteps = element.getSteps();
		List<TestResult> testResults = new ArrayList<TestResult>();
		for (Step step : allSteps) {
			executedSteps.add(step);
			if (step.getStatus() == Status.FAILED) {
				List<String> failedTestRailIds = null;
				for (int i = executedSteps.size() - 1; i >= 0; i--) {
					failedTestRailIds = extractTestRefIds(executedSteps
							.get(i));
					if (failedTestRailIds.size() > 0) {
						// Break once immediate test reference id found.
						failedTestRefIds.addAll(failedTestRailIds);
						break;
					}
				}
				testResults.add(new TestResult(step, executedSteps, failedTestRailIds));				
			} else if (step.getStatus() == Status.SKIPPED) {
				skippedTestRefIds.addAll(extractTestRefIds(step));				
			}
		}
		return testResults;
	}

	private List<String> extractTestRefIds(Step step) {
		List<String> testIds = new ArrayList<String>();
		for (Comment comment : step.getComments()) {
			if ((comment.getCommentValue())
					.contains(SharedConstants.TEST_REFERENCE_COMMENT_TITLE)) {
				String testRefComment = comment.getCommentValue();
				String[] testRefIdList = testRefComment.split(",");
				for (int j = 0; j < testRefIdList.length; j++) {					
					String testRefId = testRefIdList[j].replaceFirst(
							SharedConstants.HASH_SYMBOL, "").replaceAll(
							"JIRA:", "");
					testIds.add(testRefId);					
				}
			}
		}
		return testIds;
	}

	private void processPassedTests(List<String> testRefIds) {

		passedTestRefIds = new ArrayList<String>();
		passedTestRefIds.addAll(testRefIds);
		passedTestRefIds.removeAll(failedTestRefIds);
		passedTestRefIds.removeAll(skippedTestRefIds);		
	}

	public List<String> getFailedTestReferenceIds() {

		return failedTestRefIds;
	}

	public List<String> getPassedTestReferenceIds() {

		return passedTestRefIds;
	}
}