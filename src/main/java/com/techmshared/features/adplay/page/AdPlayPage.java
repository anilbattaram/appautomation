package com.techmshared.features.adplay.page;

public interface AdPlayPage {

	public void checkAdPlay();

	public void checkMidrollAdPlayed();
	
}
