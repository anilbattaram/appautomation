package com.techmshared.features.adplay.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.techmshared.features.mvpd.util.HelperConstants;
import com.techmshared.features.videoplay.page.AndroidVideoFlowPage;
import com.techmshared.util.ApplicationHelper;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.PropertiesReader;
import com.techmshared.util.SharedConstants;

public class AndroidAdPlayPage implements AdPlayPage{
	
	private AppiumDriver driver;
	private Wait waitVar;
	PropertiesReader properties;

	private static final Logger LOGGER = Logger
			.getLogger(AndroidVideoFlowPage.class.getName());

	public AndroidAdPlayPage() {

		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		waitVar = new WebDriverWait(driver, Long.parseLong(properties
				.getProperty(HelperConstants.DRIVER_IMPLICIT_SECONDS_WAIT_KEY)));
	}

	public void checkAdPlay() {

		driver.manage().timeouts().implicitlyWait(SharedConstants.IMPILCIT_WAIT_DEFAULT, TimeUnit.MINUTES);
		WebElement adElement = driver.findElement(By.id(properties
				.getProperty(HelperConstants.ANDROID_VIDEO_AD_COUNTER_KEY)));
		Assert.assertTrue(adElement.isDisplayed());
		ApplicationHelper.sleep(SharedConstants.LOAD_WAIT_TIME);

	}

	public void checkMidrollAdPlayed() {

		try {
			driver.manage().timeouts().implicitlyWait(SharedConstants.IMPILCIT_WAIT_DEFAULT, TimeUnit.SECONDS);
			WebElement adElement = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_AD_COUNTER_KEY))));
			Assert.assertTrue(adElement.isDisplayed());

			WebElement seeker = driver
					.findElement(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_SEEKER_BAR_KEY)));
			boolean seekerPresent = seeker.isDisplayed();
			TouchAction touch = new TouchAction(driver);
			touch.tap(seeker).perform();
			int startX = seeker.getLocation().getX();
			int startY = seeker.getLocation().getY();
			int endX = startX + (75);
			driver.swipe(startX, startY, endX, startY, SharedConstants.SCROLL_MAX_DURATION);
			if (seekerPresent) {
				ApplicationHelper.navigatePageBack();
				Assert.assertTrue(false);

			}
		} catch (NoSuchElementException excep) {

			LOGGER.debug("NoSuchElementException occurred" + excep);

		}
	}	
}