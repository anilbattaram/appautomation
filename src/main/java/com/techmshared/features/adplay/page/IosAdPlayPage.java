package com.techmshared.features.adplay.page;

import io.appium.java_client.AppiumDriver;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.techmshared.features.videoplay.page.IosVideoFlowPage;
import com.techmshared.features.videoplay.page.element.IosVideoElement;
import com.techmshared.util.ApplicationHelper;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.PropertiesReader;
import com.techmshared.util.SharedConstants;

public class IosAdPlayPage implements AdPlayPage{

	private AppiumDriver driver;
	PropertiesReader properties;
	private static final Logger LOGGER = Logger
			.getLogger(IosVideoFlowPage.class.getName());
	IosVideoElement videoElement;

	public IosAdPlayPage() {
		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		videoElement = new IosVideoElement();
	}

	public void checkAdPlay() {
		WebElement adView = null;
		Wait adWait = new WebDriverWait(driver,
				SharedConstants.IMPLICIT_WAIT_TIME);
		int adWaitTimeSecs = 20;
		try {
			Wait wait = new FluentWait(driver).withTimeout(1, TimeUnit.MINUTES)
					.pollingEvery(20, TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);

			adView = videoElement.findAdViewElement(wait);

			WebElement adText = videoElement.findAdTextElement(adView);

			parseWaitTimeForAd(adText.getText());
			adWaitTimeSecs = parseWaitTimeForAd(adText.getText());
			WebElement adPlaySlider = videoElement
					.findVideoSliderElement(adWait);

			// Ad should not slide. hence when available mark as Failure.
			if (adPlaySlider.isDisplayed()) {
				Assert.assertFalse(false);
			}
			long waitMs = (long) (adWaitTimeSecs * 1000);
			ApplicationHelper.sleep(waitMs);
			WebElement fullEpisodesListTitle = videoElement
					.findFullEpisodesTitleElement();
			Assert.assertTrue(fullEpisodesListTitle.isEnabled());
		} catch (NoSuchElementException e) {
			LOGGER.debug("Exception as no element is displayed" + e);
			adWaitTimeSecs = 20;
		} catch (TimeoutException e) {
			adWaitTimeSecs = 20;
			LOGGER.debug("TimeOut exception from Ad" + e);
		} catch (Exception ex) {
			LOGGER.debug("Exception occured while trying to search for backbutton in ad"
					+ ex);
			WebElement backButton = videoElement.findBackButtonElement();
			backButton.click();
		}
	}

	public void checkMidrollAdPlayed() {
		WebElement element = getAdDisplayElement();
		try {
			if (element != null) {

				WebElement playedTimeElement = videoElement
						.findAdPlayedTimeElement();
				WebElement totalPlayTimeElement = videoElement
						.findTotalAdPlayedTimeElement();
				int playedTimeSeconds = getTimeInSeconds(playedTimeElement);
				int totalTimeSeconds = getTimeInSeconds(totalPlayTimeElement);
				int remaingTimeMinutes = (totalTimeSeconds - playedTimeSeconds) / 60;

				boolean isAdElementDisplayed = element.isDisplayed();

				while (remaingTimeMinutes > 5 && !isAdElementDisplayed) {
					element = getAdDisplayElement();

					if (element != null) {
						playedTimeElement = videoElement
								.findAdPlayedTimeElement();
						totalPlayTimeElement = videoElement
								.findTotalAdPlayedTimeElement();
						playedTimeSeconds = getTimeInSeconds(playedTimeElement);
						totalTimeSeconds = getTimeInSeconds(totalPlayTimeElement);
						remaingTimeMinutes = (totalTimeSeconds - playedTimeSeconds) / 60;
						isAdElementDisplayed = element.isDisplayed();
					}
				}

				if (remaingTimeMinutes < 5 && !isAdElementDisplayed) {
					Assert.assertTrue(false);
				} else {
					int adWaitTimeSecs = parseWaitTimeForAd(element.getText());
					long waitMs = (long) (adWaitTimeSecs * SharedConstants.MILLISECONDS_CONVERSION_KEY);
					ApplicationHelper.sleep(waitMs);
					Assert.assertTrue(true);
				}
			}
		} catch (Exception ex) {
			LOGGER.debug("Exception occured while trying to search for backbutton in ad"
					+ ex);
			WebElement backButton = videoElement.findBackButtonElement();
			backButton.click();
		}
	}

	private WebElement getAdDisplayElement() {
		WebElement adText = null;
		try {
			Wait wait = new FluentWait(driver).withTimeout(5, TimeUnit.MINUTES)
					.pollingEvery(20, TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class);

			WebElement element = videoElement.findAdViewElement(wait);

			adText = videoElement.findAdTextElement(element);

		} catch (Exception ex) {
			driver.navigate().back();
			LOGGER.debug("Return Back from Ad" + ex);
		}

		return adText;
	}

	private int getTimeInSeconds(WebElement element) {
		int time = 0;
		int minutes = 0;
		int seconds = 0;

		String elementText = element.getText();

		String[] timeBreakup = elementText.split(":");

		if (timeBreakup.length == 2) {

			minutes = Integer.valueOf(timeBreakup[0]);
			seconds = Integer.valueOf(timeBreakup[1]);
		}

		time = (minutes * 60) + seconds;

		return time;
	}

	private int parseWaitTimeForAd(String adText) {
		String[] timePart = adText.split(":");

		int bufferSeconds = 20;
		int totalTimeSeconds = bufferSeconds;

		if (timePart.length == 3) {
			int timeMinutes = Integer.valueOf(timePart[1].trim());
			int timeSeconds = Integer.valueOf(timePart[2].trim());

			totalTimeSeconds = (timeMinutes * 60) + timeSeconds + bufferSeconds;
		}

		return totalTimeSeconds;
	}
}