package com.techmshared.features.videoplay.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.techmshared.features.mvpd.util.HelperConstants;
import com.techmshared.features.videoplay.page.element.AndroidVideoElement;
import com.techmshared.util.ApplicationHelper;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.PropertiesReader;
import com.techmshared.util.SharedConstants;

public class AndroidVideoFlowPage implements VideoFlowPage {

	private AppiumDriver driver;
	private Wait waitVar;
	PropertiesReader properties;
	private AndroidVideoElement androidVideoElement;

	private static final Logger LOGGER = Logger
			.getLogger(AndroidVideoFlowPage.class.getName());

	public AndroidVideoFlowPage() {

		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		androidVideoElement = new AndroidVideoElement();
		waitVar = new WebDriverWait(driver, Long.parseLong(properties
				.getProperty(HelperConstants.DRIVER_IMPLICIT_SECONDS_WAIT_KEY)));
	}

	public void playVideo(int videoPlayTime) {

		// milliseconds conversion
		long playTime = videoPlayTime * SharedConstants.DEFAULT_LOAD_WAIT_TIME;
		// Ad buffer
		ApplicationHelper.sleep(SharedConstants.AD_BUFFER_WAIT_TIME);
		touchAction();
		double startingVideoTime = progressTime();
		ApplicationHelper.sleep(SharedConstants.VIDEO_BUFFER_WAIT_TIME);
		checkProgressOfVideo(startingVideoTime);
		WebElement videoElement = androidVideoElement
				.findVideoContainerElement();
		Assert.assertTrue(videoElement.isDisplayed());

		ApplicationHelper.sleep(playTime);
	}

	public void checkVideoPlay(int videoPlayTime) {

		long playTime = (long) (videoPlayTime * 1000);

		WebElement videoElement = androidVideoElement
				.findVideoContainerElement();
		Assert.assertTrue(videoElement.isDisplayed());
		double startingVideoTime = progressTime();

		ApplicationHelper.sleep(playTime);
		checkProgressOfVideo(startingVideoTime);
	}

	public void clickPlayPause(boolean isTouchRequired) {

		performPlayPause(isTouchRequired);
	}

	public void checkPlayPause() {

		touchAction();
		performPlayPause(true);
	}

	public void clickBackButtonAfterVideoPlay(boolean isTouchRequired) {
		// Implement Here

	}

	public void scrubVideoPlayBackward(boolean isTouchRequired) {

		try {

			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			WebElement videoElement = androidVideoElement
					.findVideoContainerElement();
			Assert.assertTrue(videoElement.isDisplayed());

			WebElement seeker = driver
					.findElement(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_SEEKER_BAR_KEY)));
			double beforeScrubVideoTimeInDouble = progressTime();
			int startX = seeker.getLocation().getX();
			int startY = seeker.getLocation().getY();
			int endX = startX + (seeker.getSize().getWidth());
			driver.swipe(endX, startY, startX, startY, 1000);
			checkBackwardVideo(beforeScrubVideoTimeInDouble);

		} catch (NoSuchElementException excep) {

			LOGGER.debug("NoSuchElementException" + excep);
			touchAction();
			WebElement videoElement = androidVideoElement
					.findVideoContainerElement();
			Assert.assertTrue(videoElement.isDisplayed());

			WebElement seeker = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_SEEKER_BAR_KEY))));
			int startX = seeker.getLocation().getX();
			int startY = seeker.getLocation().getY();
			int endX = startX + (seeker.getSize().getWidth());
			driver.swipe(endX, startY, startX, startY,
					SharedConstants.SCROLL_MAX_DURATION);
		}

	}

	public void scrubVideoPlayForward(boolean isTouchRequired) {

		try {

			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			WebElement videoElement = androidVideoElement
					.findVideoContainerElement();
			Assert.assertTrue(videoElement.isDisplayed());

			WebElement seeker = driver
					.findElement(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_SEEKER_BAR_KEY)));

			double beforeScrubVideoTimeInDouble = progressTime();
			int startX = seeker.getLocation().getX();
			int startY = seeker.getLocation().getY();
			int endX = startX + 150;
			driver.swipe(startX, startY, endX, startY, 1000);
			driver.swipe(startX + 150, startY, startX + 300, startY, 1000);
			checkProgressOfVideo(beforeScrubVideoTimeInDouble);

		} catch (NoSuchElementException exc) {

			LOGGER.debug("NoSuchElementException has occurred.." + exc);
			touchAction();
			WebElement videoElement = androidVideoElement
					.findVideoContainerElement();
			Assert.assertTrue(videoElement.isDisplayed());

			WebElement seeker = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_SEEKER_BAR_KEY))));
			int startX = seeker.getLocation().getX();
			int startY = seeker.getLocation().getY();
			int endX = startX + 150;
			driver.swipe(startX, startY, endX, startY, 1000);
			driver.swipe(startX + 150, startY, startX + 300, startY, 1000);
		}

	}

	public void scrubToVideoEnd(boolean isTouchRequired) {

		try {

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebElement seeker = driver
					.findElement(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_SEEKER_BAR_KEY)));
			Assert.assertTrue(seeker.isDisplayed());
			int startX = seeker.getLocation().getX();
			int startY = seeker.getLocation().getY();
			int endX = startX + (seeker.getSize().getWidth());
			driver.swipe(startX, startY, endX, startY, 1000);
		} catch (NoSuchElementException exception) {

			LOGGER.debug("NoSuchElementException occurred" + exception);
			touchAction();
			WebElement seeker = (WebElement) waitVar
					.until(ExpectedConditions.presenceOfElementLocated(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_SEEKER_BAR_KEY))));
			Assert.assertTrue(seeker.isDisplayed());
			int startX = seeker.getLocation().getX();
			int startY = seeker.getLocation().getY();
			int endX = startX + (seeker.getSize().getWidth());
			driver.swipe(startX, startY, endX, startY, 1000);
		}
	}

	public void clickClosedCaptions(boolean isTouchRequired) {

		try {

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebElement ccButton = androidVideoElement
					.findClosedCaptionElement();
			Assert.assertTrue(ccButton.isDisplayed());
			ccButton.click();

		} catch (TimeoutException excep) {

			LOGGER.debug("TimeoutException occurr" + excep);
			touchAction();
			WebElement ccButton = androidVideoElement
					.findPresenceClosedCaptionElement();
			Assert.assertTrue(ccButton.isDisplayed());
			ccButton.click();
		}
	}

	public void checkClosedCaptions(boolean isTouchRequired) {

		WebElement subtitle = androidVideoElement.findSubTitleElement();
		Assert.assertTrue(subtitle.isDisplayed());
		ApplicationHelper.sleep(9000);
	}

	private void touchAction() {

		WebElement mainVideoView = androidVideoElement
				.findVideoContainerElement();
		Assert.assertTrue(mainVideoView.isDisplayed());
		TouchAction touchAction = new TouchAction(driver);
		touchAction.tap(mainVideoView).perform();
	}

	private void performPlayPause(boolean isTouchRequired) {
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebElement pauseIcon = androidVideoElement.findPlayPauseElement();
			Assert.assertTrue(pauseIcon.isDisplayed());
			pauseIcon.click();
			ApplicationHelper.sleep(3000);
		} catch (NoSuchElementException ex) {

			LOGGER.debug("NoSuchElementException has occurred" + ex);

			if (isTouchRequired) {
				touchAction();
			}
			WebElement pauseIcon = androidVideoElement
					.findPresenceOfPlayPauseElement();
			Assert.assertTrue(pauseIcon.isDisplayed());
			pauseIcon.click();
			ApplicationHelper.sleep(1000);
		}
	}

	public void checkProgressOfVideo(double startingVideoTime) {

		double afterSomeTimeVideoTime = progressTime();

		if (afterSomeTimeVideoTime > startingVideoTime) {

			Assert.assertTrue(true);
		} else {

			driver.navigate().back();
			Assert.assertTrue(false);
		}

	}

	private WebElement findVideoProgressTextElement() {

		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return driver
					.findElement(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_PROGRESS_TEXT_KEY)));
		} catch (NoSuchElementException excep) {
			LOGGER.debug("NoSuchElementException....." + excep);
			WebElement mainVideoView = androidVideoElement
					.findVideoContainerElement();
			Assert.assertTrue(mainVideoView.isDisplayed());
			TouchAction touchAction = new TouchAction(driver);
			touchAction.tap(mainVideoView).perform();
			return driver
					.findElement(By.id(properties
							.getProperty(HelperConstants.ANDROID_VIDEO_PROGRESS_TEXT_KEY)));
		}
	}

	private double progressTime() {

		ApplicationHelper.sleep(SharedConstants.DEFAULT_LOAD_WAIT_TIME);
		WebElement videoProgressTextElement = findVideoProgressTextElement();
		String videoProgressText = videoProgressTextElement.getText();
		String progressTime = videoProgressText.substring(0,
				videoProgressText.indexOf("/"));
		double progressTimeInDouble = Double.parseDouble(progressTime.replace(
				":", "."));
		return progressTimeInDouble;
	}

	private void checkBackwardVideo(double beforeScrubVideoTimeInDouble) {

		double afterScrubVideoTimeInDouble = progressTime();
		if (afterScrubVideoTimeInDouble < beforeScrubVideoTimeInDouble) {

			Assert.assertTrue(true);
		} else {

			driver.navigate().back();
			Assert.assertTrue(false);
		}

	}

	public void checkVideoPause() {

		double startingVideoTime = progressTime();
		ApplicationHelper.sleep(SharedConstants.LOAD_WAIT_TIME);
		double afterSomeTimeVideoTime = progressTime();
		if (afterSomeTimeVideoTime == startingVideoTime) {
			Assert.assertTrue(true);
		} else {
			driver.navigate().back();
			Assert.assertTrue(false);
		}

	}

}