package com.techmshared.features.videoplay.page.element;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.techmshared.features.mvpd.util.HelperConstants;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.PropertiesReader;

public class AndroidVideoElement {

	private AppiumDriver driver;
	private Wait waitVar;
	PropertiesReader properties;
	
	public AndroidVideoElement() {
		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		waitVar = new WebDriverWait(driver, Long.parseLong(properties
				.getProperty(HelperConstants.DRIVER_IMPLICIT_SECONDS_WAIT_KEY)));
	}
	
	public WebElement findVideoContainerElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.id(properties
						.getProperty(HelperConstants.ANDROID_VIDEO_CONTAINER_KEY))));
	}

	public WebElement findSubTitleElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.ANDROID_VIDEO_SUBTITLE_KEY))));
	}

	public WebElement findPresenceClosedCaptionElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.ANDROID_VIDEO_CC_BUTTON_KEY))));
	}

	public WebElement findClosedCaptionElement() {
		return driver.findElement(By.xpath(properties
				.getProperty(HelperConstants.ANDROID_VIDEO_CC_BUTTON_KEY)));
	}

	public WebElement findPresenceOfPlayPauseElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.id(properties
						.getProperty(HelperConstants.ANDROID_VIDEO_PLAYPAUSE_BUTTON_KEY))));
	}

	public WebElement findPlayPauseElement() {
		return driver
				.findElement(By.id(properties
						.getProperty(HelperConstants.ANDROID_VIDEO_PLAYPAUSE_BUTTON_KEY)));
	}
}