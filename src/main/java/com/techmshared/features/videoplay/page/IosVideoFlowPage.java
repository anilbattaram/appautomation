package com.techmshared.features.videoplay.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.techmshared.features.mvpd.util.HelperConstants;
import com.techmshared.features.videoplay.page.element.IosVideoElement;
import com.techmshared.util.ApplicationHelper;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.PropertiesReader;
import com.techmshared.util.SharedConstants;

public class IosVideoFlowPage implements VideoFlowPage {

	private AppiumDriver driver;
	PropertiesReader properties;
	private Wait waitVar;
	private static final Logger LOGGER = Logger
			.getLogger(IosVideoFlowPage.class.getName());
	IosVideoElement videoElement;

	public IosVideoFlowPage() {
		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		videoElement = new IosVideoElement();
		waitVar = new WebDriverWait(driver, Long.parseLong(properties
				.getProperty(HelperConstants.DRIVER_IMPLICIT_SECONDS_WAIT_KEY)));
	}

	public void playVideo(int playDuration) {
		try {
			driver.manage()
					.timeouts()
					.implicitlyWait(SharedConstants.IMPILCIT_WAIT_DEFAULT,
							TimeUnit.SECONDS);
			WebElement playFromBeginning = videoElement
					.findVideoPlayBeginingElement();
			playFromBeginning.click();
		} catch (NoSuchElementException e) {
			LOGGER.debug("Exception occurred as no element is displayed" + e);
		} catch (TimeoutException e) {
			LOGGER.debug("TimeOut exception " + e);
		}

		try {
			Wait wait = new FluentWait(driver).withTimeout(playDuration, TimeUnit.SECONDS)
					.pollingEvery(20, TimeUnit.MILLISECONDS)
					.ignoring(NoSuchElementException.class);
			WebElement adView = videoElement.findAdViewElement(wait);
			if (adView.isEnabled()) {
				WebElement adText = videoElement.findAdTextElement(adView);
				Assert.assertTrue(adView.isEnabled());
				ApplicationHelper.sleep(SharedConstants.VIDEO_BUFFER_WAIT_TIME);
				ApplicationHelper.sleep(SharedConstants.VIDEO_BUFFER_WAIT_TIME);
				WebElement mainVideoView = videoElement.findMainVideoViewElement();
				Assert.assertTrue(mainVideoView.isEnabled());
				if(!adText.isDisplayed()) {
					checkProgressOfVideo(10);
				} else { // to handle Ad freeze
					WebElement backButton = videoElement.findAdBackButtonElement();
					backButton.click();
					Assert.assertTrue(false);
				}				
				
			} else {
				// NO AD.... check progress... for clips
				checkProgressOfVideo(10);
			}			
		}catch (TimeoutException e) {
			LOGGER.debug("Exception if video element didn't play " + e);
			checkProgressOfVideo(10);
		}catch (Exception e) {
			LOGGER.debug("Exception if video element didn't play " + e);
			clickBackButtonAfterVideoPlay(true);
			Assert.assertTrue(false);
		}
	}
	
	public void checkVideoPlay(int videoPlayTime) {
		
		ApplicationHelper.sleep(SharedConstants.LOAD_WAIT_TIME);
		long playTime = (long) (videoPlayTime * SharedConstants.MILLISECONDS_CONVERSION_KEY);
		ApplicationHelper.sleep(playTime);
		WebElement mainVideoView = videoElement.findMainVideoViewElement();
		Assert.assertTrue(mainVideoView.isEnabled());
	}

	public void clickPlayPause(boolean isTouchRequired) {
		WebElement mainVideoView = videoElement.findMainVideoViewElement();

		if (isTouchRequired) {

			TouchAction touch = new TouchAction(driver);
			touch.tap(mainVideoView).perform();
		}

		WebElement playButton = videoElement.findVideoPlayElement();
		Assert.assertTrue(playButton.isEnabled());
		playButton.click();
	}

	public void checkPlayPause() {
		WebElement playButton = videoElement.findVideoPlayElement();
		Assert.assertTrue(playButton.isEnabled());
	}

	public void clickBackButtonAfterVideoPlay(boolean isTouchRequired) {
		try {
			WebElement mainVideoView = videoElement.findMainVideoViewElement();

			if (isTouchRequired) {
				TouchAction touch = new TouchAction(driver);
				touch.tap(mainVideoView).perform();
			}

			WebElement backButton = videoElement.findVideoBackButtonElement();
			backButton.click();
		} catch (Exception e) {
			LOGGER.debug("Exception as no element is displayed" + e);
		}
	}

	public void clickClosedCaptions(boolean isTouchRequired) {
		WebElement mainVideoView = videoElement.findFullEpisodeViewElement();

		if (isTouchRequired) {

			TouchAction touch = new TouchAction(driver);
			touch.tap(mainVideoView).perform();
		}

		WebElement playerSettings = videoElement
				.findVideoPlayerSettingsElement();

		Assert.assertTrue(playerSettings.isDisplayed());
		playerSettings.click();

		WebElement ccButton = videoElement.findVideoPlayerCCElement();
		Assert.assertTrue(ccButton.isDisplayed());
		ccButton.click();
	}

	public void checkClosedCaptions(boolean isTouchRequired) {
		WebElement mainVideoView = videoElement.findMainVideoViewElement();
		if (isTouchRequired) {
			TouchAction touch = new TouchAction(driver);
			touch.tap(mainVideoView).perform();
		}
		Assert.assertTrue(mainVideoView.isEnabled());
	}

	public void scrubVideoPlayBackward(boolean isTouchRequired) {
		WebElement mainVideoView = videoElement.findFullEpisodeViewElement();

		if (isTouchRequired) {
			TouchAction touch = new TouchAction(driver);
			touch.tap(mainVideoView).perform();
		}

		WebElement playSlider = videoElement.findVideoPlayerSliderElement();

		ApplicationHelper.swipeBackward(playSlider, 30);
		ApplicationHelper.sleep(SharedConstants.PAGE_LOAD_WAIT_TIME);
	}

	public void scrubVideoPlayForward(boolean isTouchRequired) {
		
		WebElement mainVideoView = videoElement.findFullEpisodeViewElement();
		if (isTouchRequired) {
			TouchAction touch = new TouchAction(driver);
			touch.tap(mainVideoView).perform();
		}

		WebElement playSlider = videoElement.findVideoPlayerSliderElement();
		ApplicationHelper.swipeBackward(playSlider, 5);
		ApplicationHelper.sleep(SharedConstants.PAGE_LOAD_WAIT_TIME);
		ApplicationHelper.swipeHorizontally(playSlider, (playSlider.getSize()
				.getWidth())/2);
	}

	public void scrubToVideoEnd(boolean isTouchRequired) {
		WebElement mainVideoView = videoElement.findFullEpisodeViewElement();

		if (isTouchRequired) {
			TouchAction touch = new TouchAction(driver);
			touch.tap(mainVideoView).perform();
		}

		WebElement playSlider = videoElement.findVideoPlayerSliderElement();

		ApplicationHelper.swipeHorizontally(playSlider, playSlider.getSize()
				.getWidth());
	}

	public void checkProgressOfVideo(int playDuration) {
		WebElement mainVideoView = videoElement.findMainVideoViewElement();
		Assert.assertTrue(mainVideoView.isEnabled());
		TouchAction touch = new TouchAction(driver);
		touch.tap(mainVideoView).perform();
		WebElement videoPlayTime = (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By
						.xpath(properties
								.getProperty(HelperConstants.IOS_PROGRESS_TIME_KEY))));
		String beforeVideoProgress = videoPlayTime.getAttribute("value");
		ApplicationHelper.sleep(SharedConstants.PAGE_LOAD_WAIT_TIME);
		ApplicationHelper.sleep(playDuration);
		String afterVideoProgress = videoPlayTime.getAttribute("value");
		if (!(beforeVideoProgress.equalsIgnoreCase(afterVideoProgress))) {
			Assert.assertTrue(true);
		}
		touch.tap(mainVideoView).perform();		
	}

	public void checkProgressOfVideo(double startingVideoTime) {
		// Implement Here
		
	}

	public void checkVideoPause() {
		// Implement Here
		
	}	
}