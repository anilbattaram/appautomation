package com.techmshared.features.videoplay.page.element;

import java.util.concurrent.TimeUnit;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.techmshared.features.mvpd.util.HelperConstants;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.PropertiesReader;
import com.techmshared.util.SharedConstants;

public class IosVideoElement {

	private AppiumDriver driver;
	private Wait waitVar;
	PropertiesReader properties;

	public IosVideoElement() {

		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		waitVar = new WebDriverWait(driver, Long.parseLong(properties
				.getProperty(HelperConstants.DRIVER_IMPLICIT_SECONDS_WAIT_KEY)));
	}

	public WebElement findMainVideoViewElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_MAIN_VIDEO_VIEW_KEY))));
	}

	public WebElement findVideoPlayElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.name(properties
						.getProperty(HelperConstants.IOS_VIDEO_PLAY_BUTTON_KEY))));
	}

	public WebElement findVideoBackButtonElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.name(properties
						.getProperty(HelperConstants.IOS_VIDEO_BACK_BUTTON_KEY))));
	}

	public WebElement findVideoPlayerCCElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.id(properties
						.getProperty(HelperConstants.IOS_VIDEO_CC_SETTINGS_KEY))));
	}

	public WebElement findVideoPlayerSettingsElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.name(properties
						.getProperty(HelperConstants.IOS_VIDEO_PLAYER_SETTINGS_KEY))));
	}

	public WebElement findVideoPlayerSliderElement() {
		return (WebElement) waitVar.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_VIDEO_SLIDER_KEY))));
	}

	public WebElement findVideoSliderElement(Wait adWait) {
		return (WebElement) adWait
				.until(ExpectedConditions.presenceOfElementLocated(By.className(properties
						.getProperty(HelperConstants.IOS_VIDEO_SLIDER_CLASSNAME_KEY))));
	}

	public WebElement findAdTextElement(WebElement adView) {
		return adView.findElement(By.className(properties
				.getProperty(HelperConstants.IOS_AD_CLASSNAME_KEY)));
	}

	public WebElement findTotalAdPlayedTimeElement() {
		return driver
				.findElement(By.xpath(properties
						.getProperty(HelperConstants.IOS_AD_TOTAL_PLAYED_TIME_ELEMENT_KEY)));
	}

	public WebElement findAdPlayedTimeElement() {
		return driver.findElement(By.xpath(properties
				.getProperty(HelperConstants.IOS_AD_PLAYED_TIME_ELEMENT_KEY)));
	}

	public WebElement findAdBackButtonElement() {
		return (WebElement) waitVar.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_AD_BACK_BUTTON_KEY))));
	}

	public WebElement findAdViewElement(Wait wait) {
		return (WebElement) wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_AD_PATH_KEY))));
	}

	public WebElement findVideoPlayBeginingElement() {
		return driver
				.findElementByAccessibilityId(properties
						.getProperty(HelperConstants.IOS_VIDEO_PLAY_FROM_BEGINNING_KEY));
	}

	public WebElement findFullEpisodeViewElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_FULL_EPISODES_VIEW_KEY))));
	}

	public WebElement findFullEpisodesTitleElement() {
		return driver.findElementByAccessibilityId(properties
				.getProperty(HelperConstants.IOS_FULL_EPISODES_TITLE_KEY));
	}

	public WebElement findBackButtonElement() {
		return (WebElement) waitVar.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.IOS_BACK_BUTTON_KEY))));
	}
}