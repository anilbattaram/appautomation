package com.techmshared.features.videoplay.page;

public interface VideoFlowPage {

	public void playVideo(int playDuration);
	
	public void checkVideoPlay(int playDuration);

	public void clickPlayPause(boolean isTouchRequired);

	public void checkPlayPause();

	public void clickBackButtonAfterVideoPlay(boolean isTouchRequired);

	public void clickClosedCaptions(boolean isTouchRequired);

	public void checkClosedCaptions(boolean isTouchRequired);

	public void scrubVideoPlayBackward(boolean isTouchRequired);

	public void scrubVideoPlayForward(boolean isTouchRequired);

	public void scrubToVideoEnd(boolean isTouchRequired);	
	
	public void checkProgressOfVideo(double startingVideoTime);

	public void checkVideoPause();
	
}