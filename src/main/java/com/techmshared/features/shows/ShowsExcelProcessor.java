package com.techmshared.features.shows;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.techmshared.util.SharedConstants;

public class ShowsExcelProcessor {
	private static final Logger LOGGER = Logger
			.getLogger(ShowsExcelProcessor.class.getName());

	private String fileName;

	public ShowsExcelProcessor(String fileName) {

		this.fileName = fileName;
	}

	public List<String> processShows() {

		List<String> showNameList = new ArrayList<String>();
		String showName = null;
		Workbook workbook = getWorkBook();
		int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			Sheet sheet = workbook.getSheetAt(i);
						
			for (int j=1; j <=(sheet.getPhysicalNumberOfRows()-1); j++) {
				Row row = (Row) sheet.getRow(j);
				showName = null;
				showName = row.getCell(0).toString();				
				if (showName != null) {
					showNameList.add(showName);
				}
			}
		}

		return showNameList;
	}

	public void generateCucumberScenarioExamples() {
		
		StringBuilder value = null;
		
		try {
			List<String> showNameList = processShows();
			File file = new File(getShowFileName());
			
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(SharedConstants.EXAMPLE_VALUE);
			bw.write(SharedConstants.END_OF_LINE_SYMBOL);
			bw.write(SharedConstants.TAB_SYMBOL);
			bw.write(SharedConstants.SHOW_TEXT_FILE_HEADER);
			bw.write(SharedConstants.END_OF_LINE_SYMBOL);
			for (int i = 0; i < showNameList.size(); i++) {
				value = new StringBuilder();
				value.append(SharedConstants.PIPE_SYMBOL);
				value.append(showNameList.get(i));
				value.append(SharedConstants.PIPE_SYMBOL);

				if (value != null) {
					bw.write(SharedConstants.TAB_SYMBOL + value.toString());
					bw.write(SharedConstants.END_OF_LINE_SYMBOL);
				}
			}

			bw.close();

		} catch (IOException ex) {
			LOGGER.debug("Exception occured whlile looking for file:", ex);
		}
	}

	private Workbook getWorkBook() {

		Workbook workbook = null;

		try {
			InputStream inputFile = ShowsExcelProcessor.class.getClassLoader()
					.getResourceAsStream(fileName);
			workbook = WorkbookFactory.create(inputFile);
			inputFile.close();
		} catch (FileNotFoundException ex) {
			LOGGER.info("Exception while creating file: ", ex);
		} catch (IOException ex) {
			LOGGER.info("Exception while writing data to file: ", ex);
		} catch (InvalidFormatException ex) {
			LOGGER.info("Invalid format of xls provided input: ", ex);
		}

		return workbook;
	}

	private String processEachRow(Row row) {

		String showName = null;
		if (row.getRowNum() >= 0) {
			while (row.cellIterator().hasNext()) {
				Cell cell = (Cell) row.cellIterator().next();
				if (cell.getCellType() == Cell.CELL_TYPE_STRING
						&& cell.getColumnIndex() == 0) {					
					showName = cell.getStringCellValue();
				}
			}
		}
		return showName;
	}

	private String getShowFileName() {

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(SharedConstants.DATE_FORMAT);
		String strDate = sdf.format(cal.getTime());

		StringBuilder showsDataFileName = new StringBuilder();
		showsDataFileName.append(SharedConstants.SHOWS_LIST_DATA_FILE_NAME);
		showsDataFileName.append(strDate);
		showsDataFileName.append(SharedConstants.TEXT_FILE_EXTENSION);

		return showsDataFileName.toString();
	}
}