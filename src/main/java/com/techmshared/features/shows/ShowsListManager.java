package com.techmshared.features.shows;

import java.util.List;

import com.techmshared.features.mvpd.credential.MvpdCredentialManager;
import com.techmshared.util.ApplicationHelper;
import com.techmshared.util.SharedConstants;

public class ShowsListManager {

	String showName = null;
	ShowsExcelProcessor processor = null;
	MvpdCredentialManager manager = null;

	public ShowsListManager() {
		processor = new ShowsExcelProcessor(
				SharedConstants.SHOWS_LIST_FILE_NAME);
	}

	public String getShowName() {

		List<String> showNameList = null;
		showNameList = processor.processShows();
		int size = showNameList.size();
		int position = ApplicationHelper.getRandomNumber(size - 1);
		showName = showNameList.get(position);

		return showName;
	}

	public void generateShowData() {

		processor.generateCucumberScenarioExamples();
	}
}