package com.techmshared.features.mvpd.credential;

import java.util.List;

import com.techmshared.features.mvpd.util.MvpdAuthorization;
import com.techmshared.util.ApplicationHelper;
import com.techmshared.util.SharedConstants;

public class MvpdCredentialManager {

	private MvpdCredentialHandler handler;

	public MvpdCredentialManager() {
		
		handler = new MvpdCredentialHandler(SharedConstants.ENCRYPTED_PROVIDERS_FILE_NAME);
	}

	public MvpdAuthorization getProviderAuthorization(
			String providerName, String testScenario) {

		MvpdAuthorization authorization = null;
		List<MvpdAuthorization> authorizationList = null;

		if (providerName == null
				|| providerName
				.equalsIgnoreCase(SharedConstants.DEFAULT_PROVIDER_NAME)) {

			// Random number Logic here
			authorizationList = getEncryptedMvpdList(testScenario);
			int size = authorizationList.size();
			int position = ApplicationHelper.getRandomNumber(size - 1);
			authorization = authorizationList.get(position);
		} else {

			authorization = handler.getProviderAuthorization(providerName,
					testScenario);
		}

		return authorization;
	}

	public void generateEncryptedMvpdDetails() {

		handler.generateEncryptedMvpdDetails(SharedConstants.PROVIDERS_FILE_NAME);
	}

	public void generateMvpdData(String showName, String testScenario) {

		List<MvpdAuthorization> authorizationList = getEncryptedMvpdList(testScenario);
		handler.generateCucumberScenarioExamples(showName, testScenario, authorizationList);
	}
	
	private List<MvpdAuthorization> getEncryptedMvpdList(
			String testScenario) {

		return handler.getProviderList(testScenario);
	}
}