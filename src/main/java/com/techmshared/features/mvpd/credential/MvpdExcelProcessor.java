package com.techmshared.features.mvpd.credential;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.techmshared.features.mvpd.util.MvpdAuthorization;
import com.techmshared.util.SharedConstants;

public class MvpdExcelProcessor {
	private static final Logger LOGGER = Logger
			.getLogger(MvpdExcelProcessor.class.getName());

	public List<MvpdAuthorization> getProvidersListFromExcel(String fileName) {

		FileInputStream inputFile = null;
		List<MvpdAuthorization> providerList = null;

		try {

			inputFile = new FileInputStream(new File(fileName));

			Workbook workbook = WorkbookFactory.create(inputFile);
			providerList = processWorkBook(workbook);
			inputFile.close();
		} catch (FileNotFoundException ex) {
			LOGGER.info("Exception while creating file:", ex);
		} catch (IOException ex) {
			LOGGER.info("Exception while writing data to file:", ex);
		} catch (InvalidFormatException ex) {
			LOGGER.info("Invalid format of xls provided input: ", ex);
		}
		return providerList;
	}

	public List<MvpdAuthorization> getEncryptedListFromExcel(
			String testSenario) {

		List<MvpdAuthorization> providerList = null;

		providerList = new ArrayList<MvpdAuthorization>();
		Workbook workbook = getWorkBook(SharedConstants.ENCRYPTED_PROVIDERS_ACCESS_FILE_NAME);
		int numberOfSheets = workbook.getNumberOfSheets();

		for (int i = 0; i < numberOfSheets; i++) {
			Sheet sheet = workbook.getSheetAt(i);
			Iterator rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = (Row) rowIterator.next();
				MvpdAuthorization provider = processRow(row);
				// check and return
				if (provider != null
						&& provider.getTestSenario().contains(testSenario)) {

					providerList.add(provider);
				}
			}
		}
		return providerList;
	}

	public MvpdAuthorization getProvider(String providerName,
			String testScenario) {

		MvpdAuthorization provider = null;
		MvpdAuthorization returnProvider = null;

		Workbook workbook = getWorkBook(SharedConstants.ENCRYPTED_PROVIDERS_ACCESS_FILE_NAME);

		int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			Sheet sheet = workbook.getSheetAt(i);
			Iterator rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				provider = null;
				returnProvider = null;
				Row row = (Row) rowIterator.next();
				provider = processRow(row);
				// check and return
				if (provider != null
						&& provider.getProviderName().equalsIgnoreCase(
								providerName)
						&& provider.getTestSenario().equalsIgnoreCase(
								testScenario)) {
					returnProvider = provider;
					break;
				}
			}
		}

		return returnProvider;
	}
	
	public void postEncryptedDetailsToFile(String encryptedFileName,
			List<MvpdAuthorization> mvpdDetails) {
		
		try {
			int last = 0;
			File file = new File(encryptedFileName);
			HSSFWorkbook workbook = null;
			HSSFSheet worksheet = null;
			FileOutputStream fout = null;

			if (file.exists()) {
				file.delete();
			}

			// create new file
			fout = new FileOutputStream(file);

			workbook = new HSSFWorkbook();
			worksheet = workbook.createSheet("Providers");

			for (int i = 0; i < mvpdDetails.size(); i++) {
				last = i + 1;
				if (last != 1) {
					last = worksheet.getLastRowNum() + 1;
				} else {
					HSSFRow rowHead = worksheet.createRow(0);
					HSSFCell cellHeadA = rowHead.createCell((short) 0);
					cellHeadA
							.setCellValue(SharedConstants.EXCEL_PROVIDER_HEADER);

					HSSFCell cellHeadB = rowHead.createCell((short) 1);
					cellHeadB
							.setCellValue(SharedConstants.EXCEL_USERNAME_HEADER);

					HSSFCell cellHeadC = rowHead.createCell((short) 2);
					cellHeadC.setCellValue(SharedConstants.EXCEL_PWD_HEADER);

					HSSFCell cellHeadD = rowHead.createCell((short) 3);
					cellHeadD
							.setCellValue(SharedConstants.EXCEL_SCENARIO_HEADER);
					
					HSSFCell cellHeadE = rowHead.createCell((short) 4);
					cellHeadE
							.setCellValue(SharedConstants.EXCEL_INDEX);
				}
				HSSFRow row = worksheet.createRow(last);
				HSSFCell cellA = row.createCell((short) 0);
				cellA.setCellValue(mvpdDetails.get(i).getProviderName());

				HSSFCell cellB = row.createCell((short) 1);
				cellB.setCellValue(mvpdDetails.get(i)
						.getEncryptProviderUserName());

				HSSFCell cellC = row.createCell((short) 2);
				cellC.setCellValue(mvpdDetails.get(i)
						.getEncryptProviderCredential());

				HSSFCell cellD = row.createCell((short) 3);
				cellD.setCellValue(mvpdDetails.get(i).getTestSenario());
				
				HSSFCell cellE = row.createCell((short) 4);
				cellE.setCellValue(mvpdDetails.get(i).getIndex());
			}
			workbook.write(fout);
			fout.flush();
			fout.close();
		} catch (FileNotFoundException ex) {
			LOGGER.info("Exception while creating file: ", ex);
		} catch (IOException ex) {
			LOGGER.info("Exception while writing data to file: ", ex);
		}
	}

	private List<MvpdAuthorization> processWorkBook(Workbook workbook) {

		List<MvpdAuthorization> providerList = new ArrayList<MvpdAuthorization>();
		MvpdAuthorization provider = null;
		int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			Sheet sheet = workbook.getSheetAt(i);
			Iterator rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				provider = null;
				Row row = (Row) rowIterator.next();
				provider = processRow(row);
				if (provider != null) {
					providerList.add(provider);
				}
			}
		}

		return providerList;
	}

	private MvpdAuthorization processRow(Row row) {
		
		MvpdAuthorization provider = null;
		
		if (row.getRowNum() > 0) {
			provider = readDataFromCell(row.cellIterator());
		}		

		return provider;
	}
	
	private MvpdAuthorization readDataFromCell(Iterator cellIterator) {	
		
		String providerName = null;
		String providerUserName = null;
		String providerCredential = null;
		String testScenario = null;
		int imageIndex = -1;
		while (cellIterator.hasNext()) {
			Cell cell = (Cell) cellIterator.next();
			if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
				if (cell.getColumnIndex() == 0) {
					providerName = cell.getStringCellValue();
				} else if (cell.getColumnIndex() == 1) {
					providerUserName = cell.getStringCellValue();
				} else if (cell.getColumnIndex() == 2) {
					providerCredential = cell.getStringCellValue();
				} else if (cell.getColumnIndex() == 3) {
					testScenario = cell.getStringCellValue();
				}
				
			}
			else if (cell.getColumnIndex() == 4 && cell.getNumericCellValue() != 0.0) {
				imageIndex = (int) cell.getNumericCellValue();
			}
		}		
		
		return new MvpdAuthorization(providerName,
				providerUserName, providerCredential, testScenario,imageIndex);
	}

	private Workbook getWorkBook(String fileName) {

		InputStream inputFile = null;
		Workbook workbook = null;

		try {
			inputFile = MvpdExcelProcessor.class
					.getClassLoader()
					.getResourceAsStream(fileName);

			workbook = WorkbookFactory.create(inputFile);
			inputFile.close();
		} catch (FileNotFoundException ex) {
			LOGGER.info("Exception while creating file: ", ex);
		} catch (IOException ex) {
			LOGGER.info("Exception while writing data to file: ", ex);
		} catch (InvalidFormatException ex) {
			LOGGER.info("Invalid format of xls provided input: ", ex);
		}

		return workbook;
	}
}