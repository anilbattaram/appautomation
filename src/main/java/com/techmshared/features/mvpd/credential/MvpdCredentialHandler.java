package com.techmshared.features.mvpd.credential;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import com.techmshared.features.mvpd.util.MvpdAuthorization;
import com.techmshared.util.SharedConstants;

public class MvpdCredentialHandler {

	private static final Logger LOGGER = Logger
			.getLogger(MvpdCredentialHandler.class.getName());
	private MvpdExcelProcessor excelProcessor = null;
	
	private String encryptedFileName;

	public MvpdCredentialHandler(String encryptedFileName) {

		this.encryptedFileName = encryptedFileName;
		excelProcessor = new MvpdExcelProcessor();
	}

	public void generateEncryptedMvpdDetails(String providerFileName) {

		LOGGER.debug("Reading Providers file :: Start");
		List<MvpdAuthorization> mvpdList = excelProcessor
				.getProvidersListFromExcel(providerFileName);
		LOGGER.debug("Reading Providers file :: Completed");

		LOGGER.debug("Writing Encrypted data to File :: Start");
		excelProcessor.postEncryptedDetailsToFile(encryptedFileName, mvpdList);
		LOGGER.debug("Writing Encrypted data to File :: Completed");
	}

	public MvpdAuthorization getProviderAuthorization(
			String providerName, String testScenario) {

		return excelProcessor.getProvider(providerName, testScenario);
	}

	public List<MvpdAuthorization> getProviderList(String testScenario) {

		return excelProcessor.getEncryptedListFromExcel(testScenario);
	}

	public void generateCucumberScenarioExamples(String showName, String testScenario,
			List<MvpdAuthorization> authorizationList) {

		try {
			File file = new File(getFileName(testScenario));

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			} else {
				file.delete();
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(SharedConstants.EXAMPLE_VALUE);
			bw.write(SharedConstants.END_OF_LINE_SYMBOL);
			bw.write(SharedConstants.TAB_SYMBOL);
			bw.write(SharedConstants.TEXT_FILE_HEADER);
			bw.write(SharedConstants.END_OF_LINE_SYMBOL);

			for (MvpdAuthorization provider : authorizationList) {
				StringBuilder value = new StringBuilder();
				value.append(SharedConstants.PIPE_SYMBOL);
				value.append(showName);
				value.append(SharedConstants.PIPE_SYMBOL);
				value.append(provider.getProviderName());
				value.append(SharedConstants.PIPE_SYMBOL);

				if (value != null) {
					bw.write(SharedConstants.TAB_SYMBOL + value.toString());
					bw.write(SharedConstants.END_OF_LINE_SYMBOL);
				}
			}

			bw.close();

		} catch (IOException ex) {
			LOGGER.debug("Exception occured whlile looking for file:", ex);
		}
	}

	private String getFileName(String testScenario) {

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(SharedConstants.DATE_FORMAT);
		String strDate = sdf.format(cal.getTime());
		StringBuilder fileName = new StringBuilder();
		if (SharedConstants.NO_AUTH_TEST_SENARIO_VALUE
				.equalsIgnoreCase(testScenario)) {

			fileName.append(SharedConstants.NO_AUTH_PROVIDER_FILE_NAME);
		} else if (SharedConstants.AUTH_TEST_SENARIO_VALUE
				.equalsIgnoreCase(testScenario)) {

			fileName.append(SharedConstants.AUTH_PROVIDER_FILE_NAME);
		}

		fileName.append(strDate);
		fileName.append(SharedConstants.TEXT_FILE_EXTENSION);

		return fileName.toString();
	}
}