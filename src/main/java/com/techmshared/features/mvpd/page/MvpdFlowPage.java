package com.techmshared.features.mvpd.page;

public interface MvpdFlowPage {
	
	public void checkProviderLoginPageDisplayed(String providerName);	 
	
	public void checkProviderLogin(String providerName, String userName, String password);
	
	public void checkInvalidCredentialsPopoutDisplayed();
}