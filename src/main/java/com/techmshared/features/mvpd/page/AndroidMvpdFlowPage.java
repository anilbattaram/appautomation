package com.techmshared.features.mvpd.page;

import io.appium.java_client.AppiumDriver;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.techmshared.features.mvpd.util.HelperConstants;
import com.techmshared.util.AesEncryptor;
import com.techmshared.util.ApplicationHelper;
import com.techmshared.util.DriverHelper;
import com.techmshared.util.PropertiesReader;
import com.techmshared.util.SharedConstants;

public class AndroidMvpdFlowPage implements MvpdFlowPage {
	AppiumDriver driver;
	private Wait waitVar;
	PropertiesReader properties;
	AesEncryptor encryptor;
	private static final Logger LOGGER = Logger
			.getLogger(AndroidMvpdFlowPage.class.getName());

	public AndroidMvpdFlowPage() {

		driver = DriverHelper.getAppiumDriver();
		properties = PropertiesReader.getInstance();
		encryptor = new AesEncryptor();
		waitVar = new WebDriverWait(driver, Long.parseLong(properties
				.getProperty(HelperConstants.DRIVER_IMPLICIT_SECONDS_WAIT_KEY)));
	}
	
	public void checkProviderLoginPageDisplayed(String providerName) {

		WebElement pageElement = findLoginPageElement();
		Assert.assertTrue(pageElement.isDisplayed());
		List<WebElement> loginElements = findLoginPageTextElements(pageElement);
		WebElement emailElement = loginElements.get(0);
		Assert.assertTrue(emailElement.isDisplayed());
		WebElement passwordElement = loginElements.get(1);
		Assert.assertTrue(passwordElement.isDisplayed());
		if (providerName
				.equalsIgnoreCase(properties
						.getProperty(HelperConstants.ANDROID_MVPD_PROVIDER_CHARTER_KEY))) {
			WebElement loginButtonElement = findCharterLoginButtonElement();
			Assert.assertTrue(loginButtonElement.isDisplayed());
		} else if (providerName
				.equalsIgnoreCase(properties
						.getProperty(HelperConstants.ANDROID_MVPD_PROVIDER_OPTIMUM_KEY))) {
			WebElement loginImageElement = findOptimumProviderLoginButtonElement();
			Assert.assertTrue(loginImageElement.isDisplayed());

		} else {
			WebElement logElement = findLoginButtonElement();
			Assert.assertTrue(logElement.isDisplayed());

		}
	}

	public void checkProviderLogin(String providerName, String userName, String password) {
		
		WebElement pageElement;
		String decryptedUserName = null;
		String decryptedPassword = null;
		try {

			decryptedUserName = encryptor.decrypt(userName);
			decryptedPassword = encryptor.decrypt(password);
			ApplicationHelper.sleep(SharedConstants.LOAD_WAIT_TIME);
			pageElement = findLoginPageElement();
			Assert.assertTrue(pageElement.isDisplayed());

		} catch (ElementNotFoundException ex) {
			// for optimum provider it is webkit view,rest of the providers it
			// is android view.
			pageElement = findOptimumProviderElement();
			Assert.assertTrue(pageElement.isDisplayed());
			LOGGER.debug("Element not found" + ex);
		} catch (Exception ex) {
			LOGGER.debug("Exception occured" + ex);
		}

		ApplicationHelper.sleep(SharedConstants.PAGE_LOAD_WAIT_TIME);
		List<WebElement> loginElements = findLoginPageTextElements();

		if (loginElements != null && !loginElements.isEmpty()) {

			int noOfElements = loginElements.size();
			if (noOfElements > 0 && noOfElements <= 2) {

				ApplicationHelper.sleep(SharedConstants.ELEMENT_LOAD_WAIT_TIME);
				WebElement emailElement = loginElements.get(0);
				Assert.assertTrue(emailElement.isDisplayed());
				WebElement passwordElement = loginElements.get(1);
				Assert.assertTrue(passwordElement.isDisplayed());
				emailElement.sendKeys(decryptedUserName);
				driver.hideKeyboard();
				driver.tap(SharedConstants.TAP_START_X,
						SharedConstants.TAP_START_Y, SharedConstants.TAP_END_X,
						SharedConstants.TAP_END_Y);
				passwordElement.sendKeys(decryptedPassword);
				driver.hideKeyboard();
			}
		} else {

			Assert.assertTrue(false);
		}

		// The login buttons are varying for different providers
		if (providerName.equalsIgnoreCase(properties
				.getProperty(HelperConstants.ANDROID_MVPD_PROVIDER_CHARTER_KEY))) {
			WebElement loginButtonElement = findCharterLoginButtonElement();
			Assert.assertTrue(loginButtonElement.isDisplayed());
			loginButtonElement.click();
		} else if (providerName.equalsIgnoreCase(properties
				.getProperty(HelperConstants.ANDROID_MVPD_PROVIDER_OPTIMUM_KEY))) {
			WebElement loginImageElement = findOptimumProviderLoginButtonElement();
			Assert.assertTrue(loginImageElement.isDisplayed());
			loginImageElement.click();
		} else {
			WebElement logElement = findLoginButtonElement();
			Assert.assertTrue(logElement.isDisplayed());
			logElement.click();
		}

	}

	public void checkInvalidCredentialsPopoutDisplayed() {
		// Not Applicable for Android

	}
	
	private WebElement findOptimumProviderLoginButtonElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.ANDROID_MVPD_PROVIDER_OPTIMUM_LOGIN_BUTTON_KEY))));
	}

	private WebElement findLoginButtonElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.className(properties
						.getProperty(HelperConstants.ANDROID_LOGIN_BUTTON_KEY))));
	}

	private List<WebElement> findLoginPageTextElements() {
		return (List<WebElement>) waitVar
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className(properties
						.getProperty(HelperConstants.ANDROID_MVPD_PROVIDER_LOGIN_PAGE_TEXT_FIELDS_KEY))));
	}

	private WebElement findOptimumProviderElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.className(properties
						.getProperty(HelperConstants.ANDROID_MVPD_PROVIDER_OPTIMUM_LOGIN_PAGE_KEY))));
	}
	
	private List<WebElement> findLoginPageTextElements(WebElement pageElement) {
		return pageElement
				.findElements(By.className(properties
						.getProperty(HelperConstants.ANDROID_MVPD_PROVIDER_LOGIN_PAGE_TEXT_FIELDS_KEY)));
	}

	private WebElement findLoginPageElement() {
		return (WebElement) waitVar.until(ExpectedConditions
				.presenceOfElementLocated(By.className(properties
						.getProperty(HelperConstants.ANDROID_LOGIN_PAGE_KEY))));
	}
	
	private WebElement findCharterLoginButtonElement() {
		return (WebElement) waitVar
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(properties
						.getProperty(HelperConstants.ANDROID_CHARTER_LOGIN_BUTTON_KEY))));
	}
}