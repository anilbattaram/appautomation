package com.techmshared.util;

import io.appium.java_client.AppiumDriver;

public class DriverHelper {

	private static AppiumDriver driver;
	private static String devicePlatForm;
	private static String deviceVariant;
	

	public static AppiumDriver getAppiumDriver() {

		return driver;
	}

	public void setAppiumDriver(AppiumDriver driver) {
		this.driver = driver;
	}
	
	public static String getDevicePlatForm() {
		
		return devicePlatForm;
	}

	public void setDevicePlatForm(String devicePlatForm) {
		this.devicePlatForm = devicePlatForm;
	}

	public static String getDeviceVariant() {
		
		return deviceVariant;
	}

	public void setDeviceVariant(String deviceVariant) {
		
		this.deviceVariant = deviceVariant;
	}
	
	public static boolean isAndroid() {
		
		return SharedConstants.ANDROID_PLATFORM.equalsIgnoreCase(devicePlatForm);
	}
}