package com.techmshared.util;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.techmshared.features.mvpd.credential.MvpdCredentialManager;
import com.techmshared.features.mvpd.util.MvpdAuthorization;
import com.techmshared.features.shows.ShowsListManager;
import com.techmshared.util.DriverHelper;

public class ApplicationHelper {
	private static final Logger LOGGER = Logger
			.getLogger(ApplicationHelper.class.getName());
	
	private ApplicationHelper() {
		
	}

	public static void swipeUp(double x, double y) {

		AppiumDriver driver = DriverHelper.getAppiumDriver();
		driver.context(SharedConstants.NATIVE_APP);
		Dimension size = driver.manage().window().getSize();
		int startX = size.width / 2;
		int endY = (int) (size.height * x);
		int startY = (int) (size.height * y);
		driver.swipe(startX, endY, startX, startY, SharedConstants.SCROLL_MAX_DURATION);
	}

	public static void swipeDown(double x, double y, int xDiffrence) {

		AppiumDriver driver = DriverHelper.getAppiumDriver();
		driver.context(SharedConstants.NATIVE_APP);
		Dimension size = driver.manage().window().getSize();
		int startX = (size.width / 2) - xDiffrence;
		int endY = (int) (size.height * x);
		int startY = (int) (size.height * y);
		driver.swipe(startX, startY, startX, endY, SharedConstants.SCROLL_MAX_DURATION);
	}

	public static void swipeHorizontally(double x, double y) {

		AppiumDriver driver = DriverHelper.getAppiumDriver();
		driver.context(SharedConstants.NATIVE_APP);
		Dimension size = driver.manage().window().getSize();
		int startY = size.height - 10;
		int endX = (int) (size.width * x);
		int startX = (int) (size.width * y);
		driver.swipe(startX, startY, endX, startY, SharedConstants.SCROLL_DEFAULT_DURATION);
	}
	
	public static void swipeLeft(double x, double y) {
		
		AppiumDriver driver = DriverHelper.getAppiumDriver();
		driver.context(SharedConstants.NATIVE_APP);
		Dimension size = driver.manage().window().getSize();
		int startY = size.height / 2;
		int endX = (int) (size.width * x);
		int startX = (int) (size.width * y);
		driver.swipe(startX, startY, endX, startY, SharedConstants.SCROLL_DEFAULT_DURATION);
	}
	
	public static void swipeHorizontally(WebElement element, int xSwipeLength) {
			
		AppiumDriver driver = DriverHelper.getAppiumDriver();
		driver.context(SharedConstants.NATIVE_APP);
		int startX = element.getLocation().getX();
		int startY = element.getLocation().getY();
		int endX = startX + xSwipeLength;
		driver.swipe(startX, startY, endX, startY, SharedConstants.SCROLL_MAX_DURATION);		
	}
	
	public static void swipeBackward(WebElement element, int xSwipeLength) {
		
		AppiumDriver driver = DriverHelper.getAppiumDriver();
		driver.context(SharedConstants.NATIVE_APP);
		int startX = element.getLocation().getX();
		int startY = element.getLocation().getY();
		int endX = startX + xSwipeLength;
		driver.swipe(endX, startY, startX, startY, SharedConstants.SCROLL_MAX_DURATION);		
	}
	
	public static void scrollTo(WebElement element) {

		JavascriptExecutor scrollDriver = DriverHelper.getAppiumDriver();
		Map<String, String> elementMap = new HashMap<String, String>();
		String id = ((MobileElement) element).getId();
		elementMap.put("element", id);
		scrollDriver.executeScript("mobile: scrollTo", elementMap);
	}

	public static void navigatePageBack() {

		DriverHelper.getAppiumDriver().navigate().back();
	}

	public static void swipe(List<WebElement> elements) {

		AppiumDriver driver = DriverHelper.getAppiumDriver();
		for (WebElement element : elements) {
			JavascriptExecutor scrollDriver = driver;
			Map<String, String> elementMap = new HashMap<String, String>();
			String id = ((MobileElement) element).getId();
			elementMap.put("element", id);
			scrollDriver.executeScript("mobile: scrollTo", elementMap);
		}
	}

	public static void sleep(long millis) {

		try {
			Thread.sleep(millis);
		} catch (InterruptedException ex) {
			LOGGER.info("InterruptedException: ", ex);
		}
	}
	
	public static String decryptValue(String value) {

		AesEncryptor decryptor = new AesEncryptor();
		String decryptedValue = null;
		decryptor.setText(value);

		try {
			decryptedValue = decryptor.decrypt();
		} catch (Exception ex) {
			LOGGER.info("Exception while decrypt content: ", ex);
		}
		return decryptedValue;
	}

	public static MvpdAuthorization getProviderAuthorization(
			String providerName, String testScenario) {

		MvpdCredentialManager providerService = new MvpdCredentialManager();

		return providerService.getProviderAuthorization(providerName,
				testScenario);
	}
	
	public static int getRandomNumber(int range) {

		int rndNumber = 0;

		if (range > 0) {
			Random random = new Random();
			rndNumber = random.nextInt(range);
		}

		return rndNumber;
	}
	
}