package com.techmshared.util;

import java.util.List;

import com.techmshared.framework.testexecution.cucumber.model.Step;

public class TestResult {

	private Step step;
	private List<Step> executedSteps;
	private List<String> testReferenceIds;

	public TestResult(Step step, List<Step> executedSteps,
			List<String> testReferenceIds) {

		this.step = step;
		this.executedSteps = executedSteps;
		this.testReferenceIds = testReferenceIds;
	}

	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public List<Step> getExecutedSteps() {
		return executedSteps;
	}

	public void setExecutedSteps(List<Step> executedSteps) {
		this.executedSteps = executedSteps;
	}

	public List<String> getTestReferenceIds() {
		return testReferenceIds;
	}

	public void setTestReferenceIds(List<String> testReferenceIds) {
		this.testReferenceIds = testReferenceIds;
	}
}