package com.techmshared.util;

public class SharedConstants {

	public static final String ANDROID_PLATFORM = "Android";

	// Sleep Time Keys
	public static final String NATIVE_APP = "NATIVE_APP";
	public static final long DEFAULT_LOAD_WAIT_TIME = 1000;
	public static final long ELEMENT_LOAD_WAIT_TIME = 2000;
	public static final long PAGE_LOAD_WAIT_TIME = 5000;
	public static final long LOAD_WAIT_TIME = 10000;
	public static final long AD_BUFFER_WAIT_TIME = 20000;
	public static final long VIDEO_BUFFER_WAIT_TIME = 30000;
	public static final int IMPILCIT_WAIT_DEFAULT = 20;
	public static final int IMPILCIT_WAIT_LOAD = 10;
	public static final int IMPILCIT_WAIT_MAX = 100;
	public static final int BUFFER_DURATION_KEY = 10000;
	public static final int MILLISECONDS_CONVERSION_KEY = 1000;
	

	// Share Options
	public static final String FACEBOOK_SHARE_OPTION = "Facebook";
	public static final String TWITTER_SHARE_OPTION = "Twitter";
	public static final String EMAIL_SHARE_OPTION = "Email";
	public static final String LINK_SHARE_OPTION = "Link";

	// Test Rail Property File Keys
	public static final String IS_TESTRAIL_ENABLE_KEY = "IS_TESTRAIL_ENABLE";
	public static final String TESTRAIL_CLIENT_API_KEY = "TESTRAIL_CLIENT_API";
	public static final String TESTRAIL_USERNAME_KEY = "TESTRAIL_USERNAME";
	public static final String TESTRAIL_PWD_KEY = "TESTRAIL_PWD";
	public static final String TESTRAIL_SUITE_ID_KEY = "TESTRAIL_SUITE_ID";
	public static final String TESTRAIL_SEND_POST_URI_KEY = "TESTRAIL_SEND_POST_URI";
	public static final String TESTRAIL_RUN_NAME_KEY = "TESTRAIL_RUN_NAME";
	public static final String TESTRAIL_REFERENCE_PATH = "JIRA ref: [JIRA|https://tools.techmahindra.com/jira/browse/";

	// Jira Property File Keys
	public static final String IS_JIRA_ENABLE_KEY = "IS_JIRA_ENABLE";
	public static final String JIRA_BASE_URL_KEY = "JIRA_BASE_URL";
	public static final String JIRA_PATH_KEY = "JIRA_PATH";
	public static final String JIRA_USERNAME_KEY = "JIRA_USERNAME";
	public static final String JIRA_PWD_KEY = "JIRA_PWD";
	public static final String JIRA_AUTH_KEY = "JIRA_AUTH";
	public static final String JIRA_PROJECT_KEY = "JIRA_PROJECT_KEY";
	public static final String JIRA_ISSUE_TYPE_KEY = "JIRA_ISSUE_TYPE";
	public static final String JIRA_ASSIGNEE_KEY = "JIRA_ASSIGNEE";
	public static final String JIRA_PLATFORM_KEY = "JIRA_PLATFORM";
	public static final String JIRA_PRIORITY_KEY = "JIRA_PRIORITY";

	// JIRA Issue field Names
	public static final String JIRA_ISSUE_FIELD_PROJECT = "project";
	public static final String JIRA_ISSUE_FIELD_SUMMARY = "summary";
	public static final String JIRA_ISSUE_FIELD_DESCRIPTION = "description";
	public static final String JIRA_ISSUE_FIELD_ISSUETYPE = "issuetype";
	public static final String JIRA_ISSUE_FIELD_PRIORITY = "priority";
	public static final String JIRA_ISSUE_FIELD_ASSIGNEE = "assignee";
	public static final String JIRA_ISSUE_FIELD_CUSTOMFIELD_PLATFORM = "customfield_10704";
	public static final String JIRA_ISSUE_FIELDS = "fields";

	// Constants for Encryptor algorithm
	public static final String ENCODING = "UTF-8";
	public static final String HASH = "SHA-1";
	public static final String ALGORITHM_NAME = "AES";
	public static final String ENCRYPTOR_KEY = "DEFAULT_ENCRYPTOR_KEY_1234";

	// Constants for Excel processing
	public static final String PROVIDERS_FILE_NAME = "mvpdMaster.xlsx";
	public static final String ENCRYPTED_PROVIDERS_FILE_NAME = "mvpd-encrypted-credentials.xls";
	public static final String ENCRYPTED_PROVIDERS_ACCESS_FILE_NAME = "resources/mvpd-encrypted-credentials.xls";
	public static final String AUTH_PROVIDER_FILE_NAME = "authZ_providerdetails-";
	public static final String NO_AUTH_PROVIDER_FILE_NAME = "no-authZ_providerdetails-";
	public static final String TEXT_FILE_EXTENSION = ".txt";
	public static final String NO_AUTH_TEST_SENARIO_VALUE = "authZ/ no authZ";
	public static final String AUTH_TEST_SENARIO_VALUE = "authZ/ authZ";
	public static final String DEFAULT_PROVIDER_NAME = "AUTO";
	
	public static final String SHOWS_LIST_FILE_NAME = "resources/Showslist.xls";
	public static final String SHOWS_LIST_DATA_FILE_NAME = "showslist-";

	public static final String CUCUMBER_REPORT_FILE = "target/cucumber-report.json";
	public static final String CAPABILITY_CONFIG_FILE = "capability-config.properties";
	public static final String SHARED_CONFIG_FILE = "resources/mvpdelement-config.properties";
	public static final String SHARED_SIMULATOR_CONFIG_FILE = "resources/mvpdelement-simulator-config.properties";
	public static final String SHARED_DEVICE_CONFIG_FILE = "resources/mvpdelement-device-config.properties";
	public static final String JIRA_CONFIG_FILE = "resources/jira-config.properties";
	public static final String TESTRAIL_CONFIG_FILE = "resources/testrail-config.properties";

	public static final String TEST_REFERENCE_COMMENT_TITLE = "#JIRA";
	public static final String TEST_RUN_NAME_PREFIX = "Test Run for";
    public static final String COLON_SYMBOL = ":";
	public static final String HASH_SYMBOL = "#";
	public static final String END_OF_LINE_SYMBOL = "\n";
	public static final String TAB_SYMBOL = "\t";
	public static final String PIPE_SYMBOL = "|";
	public static final String SLASH_SYMBOL = "/";
	public static final String CLOSE_SQUARE_BRACKETS_SYMBOL = "]";
	public static final String ERROR_MESSAGE_VALUE = "Error Message:";
	public static final String EXAMPLE_VALUE = "Examples:";
	public static final String TEXT_FILE_HEADER = "|Show|Provider|";
	public static final String SHOW_TEXT_FILE_HEADER = "|Show|";
	public static final String JIRA_KEY_FIELD = "key";
	public static final String JIRA_NAME_FIELD = "name";
	public static final String JIRA_VALUE_FIELD = "value";
	public static final String EXCEL_PROVIDER_HEADER = "Provider";
	public static final String EXCEL_USERNAME_HEADER = "User Name";
	public static final String EXCEL_PWD_HEADER = "Password";
	public static final String EXCEL_SCENARIO_HEADER = "Test Scenario";
	public static final String DATE_FORMAT = "MM-dd-yyyy";
	public static final String EXCEL_INDEX = "Index";
	public static final String TESTRAIL_DATE_FORMAT = "MM/dd/yyyy HH:mm";

	public static final String TESTRAIL_NAME_FIELD = "name";
	public static final String TESTRAIL_SUITE_ID_FIELD = "suite_id";
	public static final String TESTRAIL_STATUS_ID_FIELD = "status_id";
	public static final String TESTRAIL_CASE_ID_FIELD = "case_ids";
	public static final String TESTRAIL_INCULDE_ALL_FIELD = "include_all";
	public static final String TESTRAIL_ID_FIELD = "id";

	public static final int SCROLL_DEFAULT_DURATION = 200;
	public static final int SCROLL_MAX_DURATION = 1000;
	public static final int TAP_START_X = 10;
	public static final int TAP_END_X = 500;
	public static final int TAP_START_Y = 50;
	public static final int TAP_END_Y = 150;
	public static final int IMPLICIT_WAIT_TIME = 20;
	
	//Defect Management title(s)
	public static final String DEFECT_MANAGMENT_JIRA_TITLE = "JIRA";
	
	//Test case Management title(s)
	public static final String TESTCASE_MANAGMENT_TESTRAIL_TITLE = "TESTRAIL";

	private SharedConstants() {

	}
}
