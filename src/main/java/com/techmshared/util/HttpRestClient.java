package com.techmshared.util;

import gherkin.deps.net.iharder.Base64;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import com.techmshared.framework.defectmanagement.jira.JiraPropertiesReader;

public class HttpRestClient {

	String url = null;
	String path = null;
	HttpClient httpClient = null;
	HttpPost post = null;
	HttpResponse response = null;
	StringBuilder sb = null;
	String userpassword = null;
	String encodedAuthorization = null;
	JiraPropertiesReader jiraProperties = null;
	private static final Logger LOGGER = Logger.getLogger(HttpRestClient.class);

	public HttpRestClient(String url, String path) {
		try {
			httpClient = HttpClientBuilder.create().build();
			this.url = url;
			this.path = path;
			jiraProperties = JiraPropertiesReader.getInstance();
			userpassword = ApplicationHelper.decryptValue(jiraProperties
					.getPropertyValue(SharedConstants.JIRA_USERNAME_KEY))
					+ ":"
					+ ApplicationHelper.decryptValue(jiraProperties
							.getPropertyValue(SharedConstants.JIRA_PWD_KEY));
			encodedAuthorization = "Basic "
					+ Base64.encodeBytes(userpassword.getBytes());			

		} catch (Exception ex) {
			LOGGER.debug("Exception :" + ex);
		}
	}

	public String postJsonRequest(String jsonStr) {

		post = new HttpPost(url + path);
		post.setHeader("Content-type", "application/json");
		post.setHeader("Authorization", encodedAuthorization);
		String responseCode = "000";
		try {
			HttpEntity jsonEntity = new StringEntity(jsonStr);
			post.setEntity(jsonEntity);
			response = httpClient.execute(post);
			responseCode = String.valueOf(response.getStatusLine()
					.getStatusCode());
		} catch (Exception ex) {
			LOGGER.debug("Exception at post Request : " + ex);
		}
		LOGGER.info("postJsonData :: Response Code : " + responseCode);
		System.out.println("responseCode: "+responseCode);
		return responseCode;
	}

	public String getRequest(String requestParamStr) {

		HttpGet get = new HttpGet(url + path + requestParamStr);
		get.setHeader("Content-type", "application/json");
		get.setHeader("Authorization", encodedAuthorization);
		String responseCode = "000";

		try {
			response = httpClient.execute(get);
			responseCode = String.valueOf(response.getStatusLine()
					.getStatusCode());
		} catch (Exception ex) {
			LOGGER.debug("Exception at Get Request : " + ex);
		}
		return responseCode;
	}

	// Reader for both post & get requests
	public Reader getReader() {
		InputStreamReader isr = null;
		BufferedReader br = null;
		try {
			isr = new InputStreamReader(response.getEntity().getContent());
			br = new BufferedReader(isr);
		} catch (Exception ex) {
			LOGGER.debug("Exception occured : " + ex);
		}
		return br;
	}

	// Response in String for both post & get requests
	public String getResponseAsString() {
		InputStreamReader isr = null;
		BufferedReader br = null;
		sb = new StringBuilder();

		try {
			isr = new InputStreamReader(response.getEntity().getContent());
			br = new BufferedReader(isr);
			String line = null;

			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (Exception ex) {
			LOGGER.debug("Exception at Response: " + ex);
		}
		return sb.toString();
	}
}