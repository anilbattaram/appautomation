package com.techmshared.util;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class CapabilitiesReader {

	private static CapabilitiesReader instance = null;
	private static Properties prop = null;
	private static final Logger LOGGER = Logger
			.getLogger(CapabilitiesReader.class.getName());
	
	private CapabilitiesReader() {

	}

	public static CapabilitiesReader getInstance() {
		if (instance == null) {
			instance = new CapabilitiesReader();
			loadProperties();
		}
		return instance;
	}

	public static void loadProperties() {

		prop = new Properties();

		try {
			FileReader inputStream = new FileReader(SharedConstants.CAPABILITY_CONFIG_FILE);
			if (inputStream != null) {
				prop.load(inputStream);
			}

		} catch (IOException ex) {
			LOGGER.debug("Unable to locate config.properties file.", ex);
		}
	}

	public String getProperty(String property) {

		String propertyValue = null;

		if (prop != null) {

			propertyValue = prop.getProperty(property);
		}

		return propertyValue;
	}
}