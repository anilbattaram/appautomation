package com.techmshared.util;

import io.appium.java_client.AppiumDriver;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class AesEncryptor {

	private static final String ENCODING = SharedConstants.ENCODING;
	private static final String HASH = SharedConstants.HASH;

	private String algorithmName = SharedConstants.ALGORITHM_NAME;
	private String encryptorKey = SharedConstants.ENCRYPTOR_KEY;
	private String text;
	private static final Logger LOGGER = Logger.getLogger(AppiumDriver.class
			.getName());

	public AesEncryptor() {

	}

	public AesEncryptor(String text, String encryptorKey) {
		this.algorithmName = SharedConstants.ALGORITHM_NAME;
		this.text = text;
		this.encryptorKey = encryptorKey;
	}

	public String encrypt() {
		return encrypt(text, encryptorKey);
	}

	public String decrypt() {
		return decrypt(text, encryptorKey);
	}

	public String decrypt(String text) {
		return decrypt(text, encryptorKey);
	}

	private String encrypt(String textToEncrypt, String encryptorKey) {
		byte[] encryptedTextBytes = null;
		try {
			SecretKeySpec keySpec = getKey(encryptorKey);
			Cipher cipher = Cipher.getInstance(algorithmName);
			cipher.init(Cipher.ENCRYPT_MODE, keySpec);
			encryptedTextBytes = cipher.doFinal(textToEncrypt
					.getBytes(ENCODING));

		} catch (Exception ex) {

			LOGGER.debug("Exception has occurred......" + ex);
		}
		return new Base64().encodeAsString(encryptedTextBytes);
	}

	private String decrypt(String textToDecrypt, String encryptorKey) {
		byte[] decryptedTextBytes = null;
		try {
			SecretKeySpec keySpec = getKey(encryptorKey);
			Cipher cipher = Cipher.getInstance(algorithmName);
			cipher.init(Cipher.DECRYPT_MODE, keySpec);
			byte[] encryptedTextBytes = Base64.decodeBase64(textToDecrypt);
			decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
		} catch (Exception ex) {
			LOGGER.debug("Exception occurred......" + ex);
		}

		return new String(decryptedTextBytes);
	}

	private SecretKeySpec getKey(String myKey) {
		SecretKeySpec secretKey = null;
		try {
			byte[] key = myKey.getBytes(ENCODING);
			key = make16(key);
			secretKey = new SecretKeySpec(key, algorithmName);
		} catch (Exception ex) {
			LOGGER.debug("Exception is occurred......" + ex);
		}
		return secretKey;
	}

	private byte[] make16(byte[] key) {
		byte[] result = null;
		byte[] keyValue = key;
		try {
			MessageDigest sha = MessageDigest.getInstance(HASH);
			keyValue = sha.digest(keyValue);
			result = Arrays.copyOf(keyValue, 16); // use only first 128 bit
		} catch (NoSuchAlgorithmException ex) {
			LOGGER.debug("NoSuchAlgorithmException..." + ex);
		}
		return result;
	}

	public void setText(String text) {
		this.text = text;
	}
}